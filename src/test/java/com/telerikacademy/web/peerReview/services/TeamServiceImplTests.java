package com.telerikacademy.web.peerReview.services;

import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.DuplicateEntityException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Role;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.repositories.AbstractReadRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.BaseReadRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.peerReview.Helper.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class TeamServiceImplTests {

    public static final String MOCK_FILE_NAME = "MOCK_FILE_NAME";
    public static final int STATUS_ID_NOT_PENDING_AND_NOT_UNDER_REVIEW = 2;
    public static final int STATUS_PENDING_ID = 1;
    public static final int UNDER_REVIEW_STATUS_ID = 3;
    public static final int REJECTED_STATUS_ID = 5;
    public static final String MOCK_COMMENT = "Mock Comment";
    public static final int STATUS_ID_ACCEPTED = 4;
    public static final String STATUS_NAME_ACCEPTED = "Accepted";
    public static final String STATUS_NAME_PENDING = "Pending";

    public static final int ADMINISTRATOR_ID = 1;
    public static final int NOT_ADMINISTRATOR_ROLE_ID = 222;
    public static final String RANDOM_TEAM_NAME_1 = "RandomTeamName734bxpaf43";

    @Mock
    UserRepository mockUserRepository;

    @Mock
    WorkItemRepository mockWorkItemRepository;

    @Mock
    FileStore fileStore;

    @Mock
    TeamRepository mockRepository;

    @Mock
    UserService mockUserService;

    @Mock
    BaseReadRepository mockBaseReadRepository;

    @Mock
    AbstractReadRepository mockAbstractReadRepository;

    @Mock
    User user;

    @InjectMocks
    TeamServiceImpl service;


    @Test
    void getAll_should_callRepository_if_Administrator() {
        // Arrange
        User user = createMockUser();
        Role administrator = new Role();
        administrator.setId(ADMINISTRATOR_ID);
        administrator.setRole("Administrator");
        user.setRole(administrator);
        // Act
        service.getAll(user);
        // Assert
        verify(mockRepository, times(1))
                .getAll();
    }

//    @Test
//    void getAll_should_Throw_if_NOTAdministrator() {
//        // Arrange
//        User user = createMockUser();
//        Role notAdministrator = new Role();
//        notAdministrator.setId(NOT_ADMINISTRATOR_ROLE_ID);
//        notAdministrator.setRole("NotAdministrator");
//        user.setRole(notAdministrator);
//        // Act
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.getAll(user));
//    }


    @Test
    void getAllTeamMembers_should_callRepository_if_User_IS_Member() {
        // Arrange
        User user = createMockUser();
        Role notAdministrator = new Role();
        notAdministrator.setId(NOT_ADMINISTRATOR_ROLE_ID);
        notAdministrator.setRole("NotAdministrator");
        user.setRole(notAdministrator);
        Team team = createMockTeam();
        when(mockRepository.getAllTeamMembers(team.getId()))
                .thenReturn(new ArrayList<>());
        TeamServiceImpl spyService = spy(service);
        doReturn(team).when(spyService).getById(team.getId());
        // Act
        spyService.getAllTeamMembers(user, team.getId());
        // Assert
        verify(mockRepository, times(1))
                .getAllTeamMembers(team.getId());
    }

//    @Test
//    void getAllTeamMembers_should_Throw_if_User_NOT_Member() {
//        // Arrange
//        User user = createMockUser();
//        User user2 = createMockUser2();
//        Role notAdministrator = new Role();
//        notAdministrator.setId(NOT_ADMINISTRATOR_ROLE_ID);
//        notAdministrator.setRole("NotAdministrator");
//        user.setRole(notAdministrator);
//        Team team = createMockTeam();
//        team.setOwner(user2);
//        when(mockUserService.userIsMemberOfTeam(team, user))
//                .thenReturn(false);
//        TeamServiceImpl spyService = spy(service);
//        doReturn(team).when(spyService).getById(team.getId());
//        // Act and Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> spyService.getAllTeamMembers(user, team.getId()));
//    }

    @Test
    void getById_should_callRepository() {
        // Arrange
        Team team = createMockTeam();
        when(mockRepository.getById(team.getId()))
                .thenReturn(team);
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.getById(team.getId());
        // Assert
        verify(mockRepository, times(1))
                .getById(team.getId());
    }

    @Test
    void getByTeamName_should_callRepository() {
        // Arrange
        Team team = createMockTeam();
        when(mockRepository.getByTeamName(team.getName()))
                .thenReturn(team);
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.getByTeamName(team.getName());
        // Assert
        verify(mockRepository, times(1))
                .getByTeamName(team.getName());
    }

    @Test
    void create_should_callRepository() {
        // Arrange
        Team team1 = createMockTeam();
        team1.setName(RANDOM_TEAM_NAME_1);
        User userOwner = createMockUser();
        userOwner.setId(RANDOM_OWNER_ID);
        team1.setOwner(userOwner);
        List<Integer> membersId = new ArrayList<>();
        User user1 = createMockUser();
        User user2 = createMockUser2();
        membersId.add(user1.getId());
        membersId.add(user2.getId());
        Invitation invitation = new Invitation();
        invitation.setTeam(team1);
        invitation.setMember(user2);
        invitation.setId(RANDOM_ATTACHMENT_ID);
        List<Invitation> nelList = new ArrayList();
        nelList.add(invitation);
        userOwner.setInvitations(nelList);
        when(mockRepository.getByTeamName(team1.getName()))
                .thenThrow(EntityNotFoundException.class);

        when(mockUserService.getById(userOwner.getId()))
                .thenReturn(userOwner);


        TeamServiceImpl spyService = spy(service);
        doNothing().when(spyService).addMembersToTeam(membersId, team1);
        // Act
        spyService.create(team1, membersId);
        // Assert
        verify(mockRepository, times(1))
                .create(team1);
    }

    @Test
    void create_shouldThrow_if_DuplicateExists() {
        // Arrange
        Team team1 = createMockTeam();
        team1.setName(RANDOM_TEAM_NAME_1);
        List<Integer> membersId = new ArrayList<>();
        User user1 = createMockUser();
        User user2 = createMockUser2();
        membersId.add(user1.getId());
        membersId.add(user2.getId());
        when(mockRepository.getByTeamName(team1.getName()))
                .thenReturn(team1);
//        when(user.isAdministrator())
//                .thenReturn(true);
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(team1, membersId));
    }

    @Test
    void addMembersToTeam_should_callRepository() {
        // Arrange
        Team team = createMockTeam();
        List<Integer> membersId = new ArrayList<>();
        User user1 = createMockUser();
        User user2 = createMockUser2();
        membersId.add(user1.getId());
        membersId.add(user2.getId());
        when(mockUserService.getById(user1.getId()))
                .thenReturn(user1);
        when(mockUserService.getById(user2.getId()))
                .thenReturn(user2);

        // Act
        service.addMembersToTeam(membersId, team);

        // Assert
        verify(mockRepository, times(2))
                .createInvitation(any());
        verify(mockUserRepository, times(2))
                .update(any());
    }

    @Test
    void addNewMembersToTeam_should_call_addMembersToTeam() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
        TeamServiceImpl spyService = spy(service);
        doReturn(team).when(spyService).getById(team.getId());
        when(mockUserService.userIsMemberOfTeam(team, user))
                .thenReturn(true);
        when(mockUserService.getById(user.getId()))
                .thenReturn(user);
        List<Integer> membersId = new ArrayList<>();
        membersId.add(user.getId());

        // Act
        spyService.addNewMembersToTeam(team.getId(), user, membersId);

        // Assert
        verify(spyService, times(1))
                .addMembersToTeam(membersId, team);
    }

    @Test
    void update_should_callRepository() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
        team.setOwner(user);

        // Act
        service.update(user, team);

        // Assert
        verify(mockRepository, times(1))
                .update(team);
    }

//    @Test
//    void update_shouldThrow_if_userNotOwner() {
//        // Arrange
//        User user = createMockUser();
//        Team team = createMockTeam();
//        team.setOwner(user);
//        User user2 = createMockUser2();
//
//        // Act and Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.update(user2, team));
//    }

    @Test
    void delete_should_callRepository() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
        team.setOwner(user);
        TeamServiceImpl spyService = spy(service);
        doReturn(team).when(spyService).getById(team.getId());
        when(mockUserService.userIsOwnerOfTeam(team, user))
                .thenReturn(true);

        spyService.delete(user, team.getId());

        // Assert
        verify(mockRepository, times(1))
                .delete(team.getId());
    }

    @Test
    void delete_shouldThrow_if_NOTOwner() {
        // Arrange
        User user = createMockUser();
        User user2 = createMockUser2();
        Team team = createMockTeam();
        team.setOwner(user2);
        TeamServiceImpl spyService = spy(service);
        doReturn(team).when(spyService).getById(team.getId());
        when(mockUserService.userIsOwnerOfTeam(team, user))
                .thenReturn(false);

        // Act and Assert
        Assertions.assertThrows(ChangesDeniedException.class,
                () -> spyService.delete(user, team.getId()));
    }

}
