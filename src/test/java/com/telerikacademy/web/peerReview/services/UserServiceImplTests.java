package com.telerikacademy.web.peerReview.services;

import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.DuplicateEntityException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.*;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.peerReview.Helper.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    public static final String SAMPLE_PHOTO_NAME_1 = "DefaultSmileyFace";
    public static final String SAMPLE_PHOTO_FILE_TYPE = "png";
    public static final int SAMPLE_PHOTO_SIZE = 22;
    public static final int SAMPLE_PHOTO_BYTES_VALUE = 2;
    public static final String RANDOM_FILE_KEY = "RandomFileKey";
    public static final int ADMINISTRATOR_ROLE_ID = 1;
    public static final int NOT_ADMINISTRATOR_ROLE = 4;

    @Mock
    UserRepository mockRepository;

    @Mock
    TeamRepository mockTeamRepository;

    @Mock
    WorkItemRepository mockWorkItemRepository;

    @Mock
    User user;

    @Mock
    MultipartFile multipartFile;

    @Mock
    FileStore fileStore;

    @Mock
    FileStoreImpl fileStoreImpl;


    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_should_callRepository_if_Administrator() {
        // Arrange
        User user = createMockUser();
        Role administratorRole = new Role();
        administratorRole.setId(ADMINISTRATOR_ROLE_ID);
        user.setRole(administratorRole);
        when(mockRepository.getAllUsers())
                .thenReturn(new ArrayList<>());
        // Act
        service.getAllUsers(user);
        // Assert
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .getAllUsers();
    }


    @Test
    void getById_should_callRepository() {
        // Arrange
        User expected = createMockUser();
        expected.setId(RANDOM_USER_ID_6);
        // Act
        service.getById(RANDOM_USER_ID_6);
        // Assert
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .getById(RANDOM_USER_ID_6);
    }


    //
//    @Test
//    void getById_should_Throw_if_NOTAdministrator() {
//        // Arrange
//        User expected = createMockCustomer();
//        expected.setId(9899876);
//        when(user.isCustomer() && user.getId() != expected.getId())
//                .thenReturn(true);
//        // ACT and Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class, ()->service.getById(user, 9899876));
//    }
//
    @Test
    void getByUsername_should_callRepository() {
        // Arrange
        User expected = createMockUser();
//        expected.setEmail("");
        when(mockRepository.getByUsername(BASIC_USERNAME_1))
                .thenReturn(expected);


        service.getByUsername(BASIC_USERNAME_1);
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .getByUsername(BASIC_USERNAME_1);
    }

    @Test
    void create_should_Throw_if_DuplicateMailExists() {
        // Arrange
        User mockUser1 = createMockUser();
        when(mockRepository.getByUsername(mockUser1.getUsername()))
                .thenReturn(mockUser1);
        // ACT and Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockUser1, Optional.empty()));
    }

    @Test
    void update_should_callRepository_if_SameUser() {
        // Arrange
        User mockUser3 = createMockUser();
        when(mockRepository.getByUsername(mockUser3.getUsername()))
                .thenReturn(mockUser3);
        when(mockRepository.getByEmail(mockUser3.getEmail()))
                .thenReturn(mockUser3);
        // Act
        service.update(mockUser3, mockUser3);
        // Assert
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .update(mockUser3);
    }

    //
//    @Test
//    void update_should_throw_if_NotSameUser() {
//        // Arrange
//        User mockUser = createMockUser();
//        when(user.isSame())
//                .thenReturn(false);
//        // ACT and Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class, ()->service.update(user, mockUser));
//    }
//
    @Test
    void update_should_throw_if_UpdatingOtherUser() {
        // Arrange
        User userToUpdate = createMockUser();
        user.setEmail(RANDOM_USER_EMAIL);
        user.setId(RANDOM_USE_ID_5);
        userToUpdate.setEmail(RANDOM_USER_EMAIL_2);
        userToUpdate.setId(RANDOM_USER_ID_4);
//        when(mockRepository.getByEmail(userToUpdate.getEmail()))
//                .thenThrow(UnauthorizedOperationException.class);
//        when(user.isCustomer())
//                .thenReturn(true);
        // ACT and Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(user, userToUpdate));
    }


    @Test
    void delete_should_callRepository_if_User_Deletes_Self() {
        // Arrange
        user = createMockUser();
        User mockUser4 = createMockUser();
        user.setId(mockUser4.getId());
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.delete(user, mockUser4.getId());
        // Assert
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .update(mockUser4);
    }

    @Test
    void delete_should_Throw_if_Id_NotMatch() {
        // Arrange
        user = createMockUser();
        User mockUser5 = createMockUser();
        user.setId(mockUser5.getId() + RANDOM_USER_ID_3);
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act and Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(user, mockUser5.getId()));
    }

    @Test
    void GetUserInvitations_should_ReturnList() {
        // Arrange
        user.addInvitationToSet(createMockInvitation());
        List<Invitation> list = user.getInvitations();
        Object[] arr = list.toArray();
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.getUserInvitations(user);
        // Assert
        Assertions.assertArrayEquals(arr, user.getInvitations().toArray());
    }


    @Test
    void userIsMemberOfTeam_should_returnTrue_if_User_IS_Member() {
        // Arrange
        User designatedUser = createMockUser();
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(true);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        Assertions.assertTrue(service.userIsMemberOfTeam(team, designatedUser));
    }


    @Test
    void respondToInvitation_should_CallRepository() {
        // Arrange
        User designatedUser = createMockUser();
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(false);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        when(mockTeamRepository.getInvitationById(invitation.getId()))
                .thenReturn(invitation);

        service.respondToInvitation(designatedUser, invitation.getId(), true);

        verify(mockTeamRepository, times(ADMINISTRATOR_ROLE_ID))
                .updateInvitation(invitation);
    }

    @Test
    void respondToInvitation_shouldThrow_if_UserNotAddressee() {
        // Arrange
        User designatedUser = createMockUser();
        designatedUser.setId(RANDOM_USER_ID);
        User incorrectUser = createMockUser();
        incorrectUser.setId(RANDOM_USER_ID_2);
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(false);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        when(mockTeamRepository.getInvitationById(invitation.getId()))
                .thenReturn(invitation);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.respondToInvitation(incorrectUser, invitation.getId(), true));

    }

    @Test
    void leaveTeam_should_CallRepository() {
        // Arrange
        User designatedUser = createMockUser();
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(true);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        when(mockWorkItemRepository.listUnresolvedItemsForReviewByThisUserInTeam(designatedUser.getId(), team.getId()))
                .thenReturn(new ArrayList<>());
        // Act
        service.leaveTeam(designatedUser, team.getId());
        // Assert
        verify(mockTeamRepository, times(ADMINISTRATOR_ROLE_ID))
                .deleteInvitation(invitation);
    }

    @Test
    void leaveTeam_shouldThrow_if_User_HasWorkItems() {
        // Arrange
        User designatedUser = createMockUser();
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(true);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        List<WorkItem> workItemsList = new ArrayList<>();
        WorkItem mockWorkItem = createMockWorkItem();
        workItemsList.add(mockWorkItem);

        when(mockWorkItemRepository.listUnresolvedItemsForReviewByThisUserInTeam(designatedUser.getId(), team.getId()))
                .thenReturn(workItemsList);

        // Act
        Assertions.assertThrows(ChangesDeniedException.class, () -> service.leaveTeam(designatedUser, team.getId()));
    }


    @Test
    void removeMemberFromTeam_should_CallRepository() {
        // Arrange
        User designatedUser = createMockUser();
        user.setId(RANDOM_OWNER_ID);
        designatedUser.setId(RANDOM_USER_ID);
        Team team = createMockTeam();
        team.setOwner(designatedUser);
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(true);
        List<Invitation> list = new ArrayList<>();
        list.add(invitation);
        designatedUser.setInvitations(list);
        when(mockTeamRepository.getById(team.getId()))
                .thenReturn(team);
        when(mockWorkItemRepository.listUnresolvedItemsForReviewByThisUserInTeam(RANDOM_USER_ID, team.getId()))
                .thenReturn(new ArrayList<>());
        UserServiceImpl spyService = Mockito.spy(service);
        Mockito.doReturn(designatedUser).when(spyService).getById(designatedUser.getId());
        // Act
        spyService.removeMemberFromTeam(team.getOwner(), designatedUser.getId(), team.getId());
        // Assert
        verify(mockTeamRepository, times(ADMINISTRATOR_ROLE_ID))
                .deleteInvitation(invitation);
    }


    @Test
    void getUserTeams_should_return_TeamOfUser() {
        // Arrange
        User designatedUser = createMockUser();
        Team team = createMockTeam();
        Invitation invitation = createMockInvitation();
        invitation.setTeam(team);
        invitation.setMember(designatedUser);
        invitation.setAccepted(true);
        List<Invitation> list1 = new ArrayList<>();
        list1.add(invitation);
        designatedUser.setInvitations(list1);
        // Act
        int size = service.getUserTeams(designatedUser).size();
        // Assert
        Assertions.assertEquals(ADMINISTRATOR_ROLE_ID, size);
    }

    @Test
    void fileIsPhoto_shouldReturn_TRUE_if_Photo() {
        // Arrange
        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return SAMPLE_PHOTO_NAME_1;
            }

            @Override
            public String getOriginalFilename() {
                return SAMPLE_PHOTO_NAME_1;
            }

            @Override
            public String getContentType() {
                return SAMPLE_PHOTO_FILE_TYPE;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return SAMPLE_PHOTO_SIZE;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[SAMPLE_PHOTO_BYTES_VALUE];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {
            }
        };


        // Assert
        Assertions.assertTrue(service.fileIsPhoto(multipartFile));
    }

    @Test
    void userIsReviewerOfWorkItem_should_returnTrue_if_User_IS_Reviewer() {
        // Arrange
        User reviewer = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(reviewer);
        Assertions.assertTrue(service.userIsReviewerOfWorkItem(workItem, reviewer));
    }


    @Test
    void updatePhoto_should_callRepository() throws IOException {
        // Arrange
        User user = createMockUser();
        MultipartFile multipartFile = createMockMultipartFile();

        UserServiceImpl spyService = Mockito.spy(service);
        Mockito.doReturn(true).when(spyService).fileIsPhoto(multipartFile);

        String fileKey = RANDOM_FILE_KEY;

        when(fileStoreImpl.keyGenerator(multipartFile.getOriginalFilename(), user, user.getId()))
                .thenReturn(fileKey);

        spyService.updatePhoto(user, multipartFile);

        verify(fileStoreImpl, times(ADMINISTRATOR_ROLE_ID))
                .uploadFile(multipartFile, fileKey);
        verify(mockRepository, times(ADMINISTRATOR_ROLE_ID))
                .update(user);
    }


}
