package com.telerikacademy.web.peerReview.services;

import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.*;
import com.telerikacademy.web.peerReview.repositories.AbstractReadRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.BaseReadRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.peerReview.Helper.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WorkItemServiceImplTests {

    public static final String MOCK_FILE_NAME = "MOCK_FILE_NAME";
    public static final int STATUS_ID_NOT_PENDING_AND_NOT_UNDER_REVIEW = 2;
    public static final int STATUS_PENDING_ID = 1;
    public static final int UNDER_REVIEW_STATUS_ID = 3;
    public static final int REJECTED_STATUS_ID = 5;
    public static final String MOCK_COMMENT = "Mock Comment";
    public static final int STATUS_ID_ACCEPTED = 4;
    public static final String STATUS_NAME_ACCEPTED = "Accepted";
    public static final String STATUS_NAME_PENDING = "Pending";

    @Mock
    UserRepository mockUserRepository;

    @Mock
    TeamRepository mockTeamRepository;

    @Mock
    FileStore fileStore;

    @Mock
    WorkItemRepository mockRepository;

    @Mock
    UserService mockUserService;

    @Mock
    BaseReadRepository mockBaseReadRepository;

    @Mock
    AbstractReadRepository mockAbstractReadRepository;

    @Mock
    User user;

    @InjectMocks
    WorkItemServiceImpl service;



    @Test
    void getAll_should_callRepository_if_Administrator() {
        // Arrange
//        User user = createMockUser();
        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.getAll();
        // Assert
        verify(mockRepository, times(1))
                .getAll();
    }

    @Test
    void listPendingWorkItemsOfTeam_should_returnItems_if_User_IS_Member() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
        when(mockUserService.userIsMemberOfTeam(team, user))
                .thenReturn(true);
        when(mockRepository.listPendingWorkItemsOfTeam(team.getId()))
                .thenReturn(new ArrayList<>());
        // Act
        service.listPendingWorkItemsOfTeam(user, team);
        // Assert
        verify(mockRepository, times(1))
                .listPendingWorkItemsOfTeam(team.getId());
    }

    @Test
    void listPendingWorkItemsOfTeam_should_Throw_if_User_NOT_Member() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
        when(mockUserService.userIsMemberOfTeam(team, user))
                .thenReturn(false);
        // Act and Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.listPendingWorkItemsOfTeam(user, team));
    }

    @Test
    void listUserCreatedWorkItems_should_returnItems_if_SameUser() {
        // Arrange
        User user = createMockUser();
        Team team = createMockTeam();
//        when(mockUserService.userIsMemberOfTeam(team, user))
//                .thenReturn(true);
        when(mockRepository.listUserCreatedWorkItems(user.getId()))
                .thenReturn(new ArrayList<>());
        // Act
        service.listUserCreatedWorkItems(user.getId());
        // Assert
        verify(mockRepository, times(1))
                .listUserCreatedWorkItems(user.getId());
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        User user = createMockUser();
//        when(mockUserService.userIsMemberOfTeam(team, user))
//                .thenReturn(true);
        when(mockRepository.filter(Optional.of(BASIC_USERNAME_1),
                        Optional.of(SAMPLE_USERNAME_1),
                        Optional.of(1), Optional.of(MOCK_TEAM_ID_1),
                        Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());
        // Act
        service.filter(user,
                Optional.of(BASIC_USERNAME_1),
                Optional.of(SAMPLE_USERNAME_1),
                Optional.of(1), Optional.of(MOCK_TEAM_ID_1),
                Optional.empty(), Optional.empty());
        // Assert
        verify(mockRepository, times(1))
                .filter(Optional.of(BASIC_USERNAME_1),
                        Optional.of(SAMPLE_USERNAME_1),
                        Optional.of(1), Optional.of(MOCK_TEAM_ID_1),
                        Optional.empty(), Optional.empty());
    }

    @Test
    void download_should_call_fileStore() throws IOException {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        workItem.setFile(MOCK_FILE_NAME);
        Team team = createMockTeam();
        MultipartFile multipartFile = createMockMultipartFile();

//        when(mockUserService.userIsMemberOfTeam(team, user))
//                .thenReturn(true);
        when(fileStore.download(workItem.getFile()))
                .thenReturn(multipartFile.getBytes());
        // Act
        service.download(user, workItem);
        // Assert
        verify(fileStore, times(1))
                .download(workItem.getFile());
    }

    @Test
    void download_should_throw_if_NOT_Reviewer() throws IOException {
        // Arrange
        User user = createMockUser();
        User user2 = createMockUser2();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        workItem.setFile(MOCK_FILE_NAME);
        Team team = createMockTeam();

        // Act and Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.download(user2, workItem));
    }

    @Test
    void download_should_throw_if_NO_File() throws IOException {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        workItem.setFile(null);
        Team team = createMockTeam();

        // Act and Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.download(user, workItem));
    }


    @Test
    void downloadAttachment_should_call_fileStore() throws IOException {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        Attachment attachment = createMockAttachment();
        attachment.setWorkItem(workItem);
        workItem.setFile(MOCK_FILE_NAME);
        Team team = createMockTeam();
        MultipartFile multipartFile = createMockMultipartFile();

//        when(mockUserService.userIsMemberOfTeam(team, user))
//                .thenReturn(true);
        when(fileStore.download(attachment.getAttachmentName()))
                .thenReturn(multipartFile.getBytes());
        // Act
        service.downloadAttachment(user, attachment);
        // Assert
        verify(fileStore, times(1))
                .download(attachment.getAttachmentName());
    }

    @Test
    void listItemsForReviewByThisUser_should_callRepository() {
        // Arrange
        User user = createMockUser();
        when(mockRepository.listItemsForReviewByThisUser(user.getId()))
                .thenReturn(new ArrayList<>());
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.listItemsForReviewByThisUser(user.getId());
        // Assert
        verify(mockRepository, times(1))
                .listItemsForReviewByThisUser(user.getId());
    }


    @Test
    void listPendingItemsForReviewByThisUser_should_callRepository() {
        // Arrange
        User user = createMockUser();
        when(mockRepository.listPendingItemsForReviewByThisUser(user.getId()))
                .thenReturn(new ArrayList<>());
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.listPendingItemsForReviewByThisUser(user.getId());
        // Assert
        verify(mockRepository, times(1))
                .listPendingItemsForReviewByThisUser(user.getId());
    }

    @Test
    void getById_should_callRepository() {
        // Arrange
        WorkItem workItem = createMockWorkItem();
        when(mockRepository.getById(workItem.getId()))
                .thenReturn(workItem);
//        when(user.isAdministrator())
//                .thenReturn(true);
        // Act
        service.getById(workItem.getId());
        // Assert
        verify(mockRepository, times(1))
                .getById(workItem.getId());
    }

    @Test
    void create_should_callRepository() {
        // Arrange
        WorkItem workItem = createMockWorkItem();
        when(mockUserService.userIsMemberOfTeam(workItem.getTeam(), workItem.getReviewer()))
                .thenReturn(true);
        // Act
        service.create(workItem, Optional.empty());
        // Assert
        verify(mockRepository, times(1))
                .create(workItem);
    }

    @Test
    void create_should_throw_if_UserNotMember() {
        // Arrange
        WorkItem workItem = createMockWorkItem();
        when(mockUserService.userIsMemberOfTeam(workItem.getTeam(), workItem.getReviewer()))
                .thenReturn(false);
        // Act and assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(workItem, Optional.empty()));
    }

    @Test
    void attach_should_callRepository() throws IOException {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        when(mockRepository.getById(workItem.getId()))
                .thenReturn(workItem);
        Attachment attachment = createMockAttachment();
        attachment.setWorkItem(workItem);
        MultipartFile multipartFile = createMockMultipartFile();

        // Act
        service.attach(user, workItem.getId(), multipartFile);

        // Assert
        verify(mockRepository, times(1))
                .createAttachment(any());
        verify(fileStore, times(1))
                .uploadFile(any(), any());
        verify(mockRepository, times(1))
                .updateAttachment(any());
    }

    @Test
    void review_should_callRepository() {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        Status status = new Status();
        int statusId = STATUS_ID_NOT_PENDING_AND_NOT_UNDER_REVIEW;
        status.setId(statusId);
        String comment = MOCK_COMMENT;
        when(mockUserService.userIsReviewerOfWorkItem(workItem, user))
                .thenReturn(true);
        when(mockRepository.getStatusById(statusId))
                .thenReturn(status);
        // Act
        service.update(user, workItem, statusId, Optional.of(comment));
        // soon to be
        //  service.review(user, workItem, statusId, Optional.of(comment));

        // Assert
        verify(mockRepository, times(1))
                .update(workItem);
    }


    @Test
    void review_shouldThrow_if_statusPending() {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        Status status = new Status();
        int statusId = STATUS_PENDING_ID;
        status.setId(statusId);
        String comment = MOCK_COMMENT;
        when(mockUserService.userIsReviewerOfWorkItem(workItem, user))
                .thenReturn(true);

        // Act and Assert
        Assertions.assertThrows(ChangesDeniedException.class,
                () -> service.update(user, workItem, statusId, Optional.of(comment)));
        // soon to be
        // Assertions.assertThrows(ChangesDeniedException.class,
        //                () -> service.review(user, workItem, statusId, Optional.of(comment)));
    }

    @Test
    void review_shouldThrow_if_statusUnderReview() {
        // Arrange
        User user = createMockUser();
        WorkItem workItem = createMockWorkItem();
        workItem.setReviewer(user);
        Status status = new Status();
        int statusId = UNDER_REVIEW_STATUS_ID;
        status.setId(statusId);
        when(mockUserService.userIsReviewerOfWorkItem(workItem, user))
                .thenReturn(true);

        // Act and Assert
        Assertions.assertThrows(ChangesDeniedException.class,
                () -> service.update(user, workItem, statusId, Optional.empty()));
        // soon to be
        //    Assertions.assertThrows(ChangesDeniedException.class,
        //                () -> service.review(user, workItem, statusId, Optional.empty()));
    }


    @Test
    void createComment_should_callRepository() {
        // Arrange
        String comment = MOCK_COMMENT;
        WorkItem workItem = createMockWorkItem();

        // Act
        service.createComment(Optional.of(comment), workItem);

        // Assert
        verify(mockRepository, times(1))
                .createComment(any());
    }

    @Test
    void delete_should_callRepository() {
        // Arrange
        User user = createMockUser();
        User user2 = createMockUser2();
        WorkItem workItem = createMockWorkItem();
        Status status = new Status();
        status.setId(STATUS_ID_ACCEPTED);
        status.setStatusName(STATUS_NAME_ACCEPTED);
        workItem.setStatus(status);
        workItem.setReviewer(user2);
        WorkItemServiceImpl spyService = Mockito.spy(service);
        Mockito.doReturn(workItem).when(spyService).getById(workItem.getId());
        // Act
        spyService.delete(user, workItem.getId());
        // Assert
        verify(mockRepository, times(1))
                .delete(workItem.getId());
    }

    @Test
    void delete_shouldThrow_if_UserNotCreator() {
        // Arrange
        User user = createMockUser();
        User user2 = createMockUser2();
        WorkItem workItem = createMockWorkItem();
        Status status = new Status();
        status.setId(STATUS_ID_ACCEPTED);
        status.setStatusName(STATUS_NAME_ACCEPTED);
        workItem.setStatus(status);
        workItem.setReviewer(user);
        workItem.setCreator(user2);
        WorkItemServiceImpl spyService = Mockito.spy(service);
        Mockito.doReturn(workItem).when(spyService).getById(workItem.getId());
        // Act

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> spyService.delete(user, workItem.getId()));
    }

    @Test
    void delete_shouldThrow_if_NOT_Accepted() {
        // Arrange
        User user = createMockUser();
        User user2 = createMockUser2();
        WorkItem workItem = createMockWorkItem();
        Status status = new Status();
        status.setId(STATUS_PENDING_ID);
        status.setStatusName(STATUS_NAME_PENDING);
        workItem.setStatus(status);
        workItem.setReviewer(user2);
        WorkItemServiceImpl spyService = Mockito.spy(service);
        Mockito.doReturn(workItem).when(spyService).getById(workItem.getId());
        // Act and Assert
        Assertions.assertThrows(ChangesDeniedException.class,
                () -> spyService.delete(user, workItem.getId()));
    }


    @Test
    void getAttachment_should_callRepository() {
        // Arrange
        Attachment attachment = createMockAttachment();
        when(mockRepository.getAttachmentById(attachment.getId()))
                .thenReturn(attachment);
        // Act
        service.getAttachmentById(attachment.getId());
        // Assert
        verify(mockRepository, times(1))
                .getAttachmentById(attachment.getId());
    }




}
