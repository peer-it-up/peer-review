package com.telerikacademy.web.peerReview;

import com.telerikacademy.web.peerReview.models.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class Helper {

    public static final int MOCK_TEAM_OWNER_ID = 5730375;
    public static final int RANDOM_OWNER_ID = 7495629;

    public static final int BASIC_USER_ID_1 = 1;
    public static final int RANDOM_USER_ID = 6283947;
    public static final int RANDOM_USER_ID_2 = 36494284;
    public static final int RANDOM_USER_ID_3 = 2978654;
    public static final int RANDOM_USER_ID_4 = 19099283;
    public static final int RANDOM_USE_ID_5 = 467823;
    public static final int RANDOM_USER_ID_6 = 9899876;

    public static final int RANDOM_REVIEWER_ID = 438947553;

    public static final String BASIC_USERNAME_1 = "username";
    public static final String SAMPLE_USER_EMAIL = "mockCustomer1@abv.bg";
    public static final String SAMPLE_USER_FIRST_NAME_1 = "George";
    public static final String SAMPLE_USER_LAST_NAME_1 = "Baker";
    public static final String SAMPLE_USER_PASSWORD_1 = "Aa!12345678";
    public static final String SAMPLE_USER_PHOTO_NAME_1 = "DefaultSmileyFace";
    public static final String SAMPLE_USER_PHONE_1 = "0123456789";
    public static final String SAMPLE_USERNAME_1 = "GeorgeSmithUsername";

    public static final String RANDOM_USER_EMAIL = "abcd";
    public static final String RANDOM_USER_EMAIL_2 = "efgh";

    public static final int MOCK_TEAM_ID_1 = 649506;
    public static final String MOCK_TEAM_NAME_1 = "MockTeamName1";

    public static final int MOCK_INVITATION_ID = 83749;


    public static final int RANDOM_WORK_ITEM_ID = 749294;
    public static final String RANDOM_WORK_ITEM_DESCRIPTION = "Random work item description here";
    public static final String SAMPLE_ATTACHMENT_NAME = "Sample attachment name";
    public static final int RANDOM_ATTACHMENT_ID = 834021357;


    public static final String SAMPLE_PHOTO_NAME_1 = "DefaultSmileyFace";
    public static final String SAMPLE_PHOTO_NAME_2 = "";
    public static final String SAMPLE_PHOTO_FILE_TYPE = "png";
    public static final int SAMPLE_PHOTO_SIZE = 22;
    public static final int SAMPLE_PHOTY_BYTES_VALUE = 2;


    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setEmail(SAMPLE_USER_EMAIL);
        mockUser.setFirstName(SAMPLE_USER_FIRST_NAME_1);
        mockUser.setLastName(SAMPLE_USER_LAST_NAME_1);
        mockUser.setPassword(SAMPLE_USER_PASSWORD_1);
        mockUser.setPhotoName(SAMPLE_USER_PHOTO_NAME_1);
        mockUser.setPhoneNumber(SAMPLE_USER_PHONE_1);
        mockUser.setActive(true);
        mockUser.setUsername(SAMPLE_USERNAME_1);
        mockUser.setId(BASIC_USER_ID_1);
        return mockUser;
    }

    public static User createMockUser2() {
        var mockUser2 = new User();
        mockUser2.setEmail(RANDOM_USER_EMAIL);
        mockUser2.setFirstName(SAMPLE_USER_FIRST_NAME_1);
        mockUser2.setLastName(SAMPLE_USER_LAST_NAME_1);
        mockUser2.setPassword(SAMPLE_USER_PASSWORD_1);
        mockUser2.setPhotoName(SAMPLE_USER_PHOTO_NAME_1);
        mockUser2.setPhoneNumber(SAMPLE_USER_PHONE_1);
        mockUser2.setActive(true);
        mockUser2.setUsername(BASIC_USERNAME_1);
        mockUser2.setId(RANDOM_USER_ID_2);
        return mockUser2;
    }

    public static Team createMockTeam() {
        User owner = createMockUser();
        owner.setId(MOCK_TEAM_OWNER_ID);
        var mockTeam = new Team();
        mockTeam.setId(MOCK_TEAM_ID_1);
        mockTeam.setName(MOCK_TEAM_NAME_1);
        mockTeam.setOwner(owner);
        return mockTeam;
    }

    public static Invitation createMockInvitation() {
        var mockInvitation = new Invitation();
        User mockUser = createMockUser();
        mockInvitation.setId(MOCK_INVITATION_ID);
        mockInvitation.setMember(mockUser);
        mockInvitation.setTeam(createMockTeam());
        return mockInvitation;
    }

    public static WorkItem createMockWorkItem() {
        var mockWorkItem = new WorkItem();
        User mockUser = createMockUser();
        User mockReviewer = createMockUser();
        mockReviewer.setId(RANDOM_REVIEWER_ID);
        Team team = createMockTeam();
        mockWorkItem.setCreator(mockUser);
        mockWorkItem.setId(RANDOM_WORK_ITEM_ID);
        mockWorkItem.setTeam(team);
        mockWorkItem.setDescription(RANDOM_WORK_ITEM_DESCRIPTION);
        mockWorkItem.setReviewer(mockReviewer);
        return mockWorkItem;
    }

    public static Attachment createMockAttachment() {
        var mockAttachment = new Attachment();
        mockAttachment.setAttachmentName(SAMPLE_ATTACHMENT_NAME);
        mockAttachment.setWorkItem(createMockWorkItem());
        mockAttachment.setId(RANDOM_ATTACHMENT_ID);
        return mockAttachment;
    }

    public static MultipartFile createMockMultipartFile() {
        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return SAMPLE_PHOTO_NAME_1;
            }
            @Override
            public String getOriginalFilename() {
                return SAMPLE_PHOTO_NAME_1;
            }
            @Override
            public String getContentType() {
                return SAMPLE_PHOTO_FILE_TYPE;
            }
            @Override
            public boolean isEmpty() {
                return false;
            }
            @Override
            public long getSize() {
                return SAMPLE_PHOTO_SIZE;
            }
            @Override
            public byte[] getBytes() throws IOException {
                return new byte[SAMPLE_PHOTY_BYTES_VALUE];
            }
            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }
            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {
            }
        };
        return multipartFile;
    }

    public static MultipartFile createMockMultipartFile2() {
        MultipartFile multipartFile = new MultipartFile() {
            @Override
            public String getName() {
                return SAMPLE_PHOTO_NAME_2;
            }
            @Override
            public String getOriginalFilename() {
                return SAMPLE_PHOTO_NAME_1;
            }
            @Override
            public String getContentType() {
                return SAMPLE_PHOTO_FILE_TYPE;
            }
            @Override
            public boolean isEmpty() {
                return false;
            }
            @Override
            public long getSize() {
                return SAMPLE_PHOTO_SIZE;
            }
            @Override
            public byte[] getBytes() throws IOException {
                return new byte[SAMPLE_PHOTY_BYTES_VALUE];
            }
            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }
            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {
            }
        };
        return multipartFile;
    }



}
