-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table peer_review.attachments: ~0 rows (approximately)
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;

-- Dumping data for table peer_review.comments: ~8 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `comment`, `work_item_id`, `timestamp`) VALUES
                                                                                  (1, 'Ima nujda ot popravka', 5, '2021-11-27 16:08:02'),
                                                                                  (2, 'You have to check a the mistake on 54 line!', 5, '2021-11-27 16:11:38'),
                                                                                  (3, 'You have to check a the mistake on 54 line!', 5, '2021-11-27 16:16:41'),
                                                                                  (4, 'You have to check a the mistake on 54 line!', 5, '2021-11-27 16:32:26'),
                                                                                  (5, 'Test comment', 5, '2021-11-27 16:42:16'),
                                                                                  (6, 'TEST COMPLETE', 5, '2021-11-27 16:47:06'),
                                                                                  (7, 'Porqza\'s comment', 6, '2021-11-27 16:49:26'),
	(8, 'Porqza\'s comment', 6, '2021-11-28 15:42:26');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table peer_review.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
                                                 (2, 'Regular User'),
                                                 (3, 'Administrator');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table peer_review.teams: ~11 rows (approximately)
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`team_id`, `name`, `owner_id`) VALUES
                                                        (1, 'DreamTeam', 1),
                                                        (2, 'Bacho Kiro', 2),
                                                        (3, 'New', 1),
                                                        (4, 'first Test create', 1),
                                                        (5, 'second Test create', 1),
                                                        (6, 'third Test create', 1),
                                                        (7, 'sixth Test create', 1),
                                                        (8, 'seventh Test create', 1),
                                                        (9, 'With working invittions', 1),
                                                        (10, 'Another test invitatins', 1),
                                                        (11, 'invitatins', 1);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Dumping data for table peer_review.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `email`, `phone_number`, `password`, `photo_name`, `role_id`, `is_active`) VALUES
                                                                                                                                                      (1, 'Nasko', 'Kurdov', 'nasko1', 'nasko@abv.bg', '0999999999', '4', 'Photo20211115', 2, 1),
                                                                                                                                                      (2, 'Nikolai', 'Porqzov', 'porqza', 'porqz@abv.bg', '0777777777', '5', 'Phodghfdd211115', 2, 1),
                                                                                                                                                      (3, 'Veselin', 'Topalov', 'ches', 'ches@abv.bg', '0111111111', '2', '123', 3, 1),
                                                                                                                                                      (4, 'Ivan', 'Nikolov', 'vanko', 'vanko1@abv.bg', '0000000000', '1', '1121221', 2, 1),
                                                                                                                                                      (5, 'Georgi', 'Veselinov', 'gosho', 'gogi@abv.bg', '0885359119', '4', 'to20211115', 2, 1),
                                                                                                                                                      (7, 'Ivo', 'G.', 'ivo', 'ivo@abv.bg', '0885359118', '4', 'to20211115', 2, 1),
                                                                                                                                                      (8, 'Hristo', 'chavdarov', 'ico', 'ico@abv.bg', '0885339118', '4', 'to20211115', 2, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table peer_review.users_teams: ~21 rows (approximately)
/*!40000 ALTER TABLE `users_teams` DISABLE KEYS */;
INSERT INTO `users_teams` (`invitation_id`, `team_id`, `member_id`, `accepted`) VALUES
                                                                                    (1, 1, 1, 0),
                                                                                    (2, 2, 2, 0),
                                                                                    (3, 1, 4, 0),
                                                                                    (4, 2, 4, 0),
                                                                                    (5, 8, 1, 0),
                                                                                    (6, 8, 2, 0),
                                                                                    (7, 8, 4, 0),
                                                                                    (8, 8, 3, 0),
                                                                                    (9, 7, 3, 1),
                                                                                    (10, 7, 2, 1),
                                                                                    (11, 7, 4, 1),
                                                                                    (12, 9, 1, 0),
                                                                                    (13, 9, 2, 0),
                                                                                    (14, 9, 3, 0),
                                                                                    (15, 9, 4, 0),
                                                                                    (16, 11, 1, 1),
                                                                                    (17, 11, 2, 1),
                                                                                    (28, 11, 4, 1),
                                                                                    (29, 11, 5, 0),
                                                                                    (30, 11, 7, 0),
                                                                                    (31, 11, 8, 1);
/*!40000 ALTER TABLE `users_teams` ENABLE KEYS */;

-- Dumping data for table peer_review.work_items: ~5 rows (approximately)
/*!40000 ALTER TABLE `work_items` DISABLE KEYS */;
INSERT INTO `work_items` (`work_item_id`, `title`, `description`, `team_id`, `creator_id`, `file`, `work_item_status_id`, `reviewer_id`) VALUES
                                                                                                                                             (2, 'Vtoro testvane na dto', 'Dto raboti li pravilno? Ili ne?....', 1, 1, 'ime na fail', 1, 2),
                                                                                                                                             (3, 'Proverka na koshera v dvora', 'Imam samnenie, che koshera v dvora na selo shte se roi', 1, 2, 'ime na fail', 1, 1),
                                                                                                                                             (4, 'Bug into customer service code', 'Ipsilon is a server and a toolkit to configure Apache-based Service Providers. The server is a pluggable selfcontained mod_wsgi application', 1, 2, 'ime na fail', 1, 1),
                                                                                                                                             (5, 'Bug into customer service code', 'Ipsilon is a server and a toolkit to configure Apache-based Service Providers. The server is a pluggable selfcontained mod_wsgi application', 1, 2, 'ime na fail', 3, 1),
                                                                                                                                             (6, 'Bug into customer service code', 'Ipsilon is a server and a toolkit to configure Apache-based Service Providers. The server is a pluggable selfcontained mod_wsgi application', 2, 1, 'ime na fail', 3, 2);
/*!40000 ALTER TABLE `work_items` ENABLE KEYS */;

-- Dumping data for table peer_review.work_item_statuses: ~5 rows (approximately)
/*!40000 ALTER TABLE `work_item_statuses` DISABLE KEYS */;
INSERT INTO `work_item_statuses` (`work_item_status_id`, `work_item_status`) VALUES
                                                                                 (1, 'Pending'),
                                                                                 (2, 'Under Review'),
                                                                                 (3, 'Change Requested'),
                                                                                 (4, 'Accepted'),
                                                                                 (5, 'Rejected');
/*!40000 ALTER TABLE `work_item_statuses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
