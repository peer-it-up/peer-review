create table roles
(
    role_id   int(1) auto_increment,
    role_name varchar(50) not null,
    constraint user_roles_id_uindex
        unique (role_id)
);

alter table roles
    add primary key (role_id);

create table users
(
    user_id      int(10) auto_increment,
    first_name   varchar(30)          not null,
    last_name    varchar(50)          not null,
    username     varchar(50)          not null,
    email        varchar(50)          not null,
    phone_number varchar(50)          not null,
    password     varchar(50)          not null,
    photo_name   varchar(50)          not null,
    role_id      int(1)     default 1 null,
    is_active    tinyint(1) default 1 not null,
    constraint users_id_uindex
        unique (user_id),
    constraint users_mail_uindex
        unique (email),
    constraint `users_phone number_uindex`
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_user_roles_id_fk
        foreign key (role_id) references roles (role_id)
);

alter table users
    add primary key (user_id);

create table teams
(
    team_id  int(20) auto_increment,
    name     varchar(50) not null,
    owner_id int         not null,
    constraint teams_id_uindex
        unique (team_id),
    constraint teams_name_uindex
        unique (name),
    constraint teams_users_user_id_fk
        foreign key (owner_id) references users (user_id)
);

alter table teams
    add primary key (team_id);

create table users_teams
(
    invitation_id int(20) auto_increment,
    team_id       int(20)              not null,
    member_id     int(20)              not null,
    accepted      tinyint(1) default 0 null,
    constraint invitations_id_uindex
        unique (invitation_id),
    constraint invitations_teams_id_fk
        foreign key (team_id) references teams (team_id),
    constraint invitations_users_id_fk_2
        foreign key (member_id) references users (user_id)
);

alter table users_teams
    add primary key (invitation_id);

create table work_item_statuses
(
    work_item_status_id int         not null
        primary key,
    work_item_status    varchar(20) not null
);

create table work_items
(
    work_item_id        int(20) auto_increment,
    title               varchar(80)      not null,
    description         text             null,
    team_id             int(20)          not null,
    creator_id          int(20)          not null,
    file                varchar(40)      null,
    work_item_status_id int(1) default 1 not null,
    reviewer_id         int(20)          not null,
    constraint work_items_id_uindex
        unique (work_item_id),
    constraint work_items_teams_id_fk
        foreign key (team_id) references teams (team_id),
    constraint work_items_users_id_fk
        foreign key (creator_id) references users (user_id),
    constraint work_items_users_user_id_fk
        foreign key (reviewer_id) references users (user_id),
    constraint work_items_work_item_statuses_id_fk
        foreign key (work_item_status_id) references work_item_statuses (work_item_status_id)
);

alter table work_items
    add primary key (work_item_id);

create table attachments
(
    attachment_id int(20) auto_increment,
    attachment    blob     not null,
    work_item_id  int(20)  not null,
    time_stamp    datetime not null,
    constraint attachments_id_uindex
        unique (attachment_id),
    constraint attachments_work_items_id_fk
        foreign key (work_item_id) references work_items (work_item_id)
);

alter table attachments
    add primary key (attachment_id);

create table comments
(
    comment_id   int(20) auto_increment
        primary key,
    comment      varchar(255) not null,
    work_item_id int(20)      not null,
    timestamp    datetime     not null,
    constraint comments_work_items_id_fk
        foreign key (work_item_id) references work_items (work_item_id)
);

