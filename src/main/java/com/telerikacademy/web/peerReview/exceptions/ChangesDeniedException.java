package com.telerikacademy.web.peerReview.exceptions;

public class ChangesDeniedException extends RuntimeException {
    public ChangesDeniedException(String message) {
        super(message);
    }
}
