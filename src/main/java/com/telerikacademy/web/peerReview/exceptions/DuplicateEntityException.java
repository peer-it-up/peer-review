package com.telerikacademy.web.peerReview.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exist.", type, attribute, value));
    }
}
