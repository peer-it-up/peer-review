package com.telerikacademy.web.peerReview.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                //.apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage(("com.telerikacademy.web.deliverIt")))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "DeliverIt",
                "Official API for Team 6 Project",
                "1.0",
                "Terms of Service -> ReadMe from GitLab?",
                new Contact("Team 6-Java33", "localhost:8080/swagger-ui.html", "DEyanAndAngel@KissAndDry.com"),
                "Api License",
                "localhost:8080/swagger-ui.html",
                Collections.emptyList());
    }

}