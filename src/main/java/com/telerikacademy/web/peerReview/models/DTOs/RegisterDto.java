package com.telerikacademy.web.peerReview.models.DTOs;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterDto {

    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols")
    @NotNull(message = "First name can't be empty")
    private String username;

    @NotEmpty
    private String password;

    @NotNull()
    private String passwordConfirm;

    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols")
    @NotNull(message = "First name can't be empty")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols")
    @NotNull(message = "Last name can't be empty")
    private String lastName;

    @Size(min = 10, max = 10, message = "Phone should be exactly 10 symbols")
    @NotNull(message = "Phone number can't be empty")
    private String phoneNumber;

    @Email
    @NotNull(message = "Email  can't be empty")
    private String email;


    public RegisterDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
