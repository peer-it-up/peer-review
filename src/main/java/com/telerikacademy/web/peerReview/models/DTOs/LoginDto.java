package com.telerikacademy.web.peerReview.models.DTOs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginDto {

    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols")
    @NotNull(message = "First name can't be empty")
    private String username;

    @NotEmpty
    @Size(min = 8, message = "password must be at least 8 characters")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
