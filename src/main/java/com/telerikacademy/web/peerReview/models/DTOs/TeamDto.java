package com.telerikacademy.web.peerReview.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class TeamDto {
    @Size(min = 3, max = 30, message = "Size of team name must be between 10 and 80 symbols")
    @NotNull
    private String name;

    private List<Integer> membersId;

    public TeamDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getMembersId() {
        return membersId;
    }

    public void setMembersId(List<Integer> membersId) {
        this.membersId = membersId;
    }
}
