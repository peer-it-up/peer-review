package com.telerikacademy.web.peerReview.models.DTOs;

import org.springframework.web.multipart.MultipartFile;

public class WorkItemFIleMvcDto extends WorkItemDto {

    private MultipartFile multipartFile;

    public WorkItemFIleMvcDto() {
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
