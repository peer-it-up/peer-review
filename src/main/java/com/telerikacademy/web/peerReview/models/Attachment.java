package com.telerikacademy.web.peerReview.models;


import javax.persistence.*;

@Entity
@Table(name = "attachments")


public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attachment_id")
    private int id;

    @Column(name = "attachment")
    private String attachmentName;

    @ManyToOne
    @JoinColumn(name = "work_item_id")
    private WorkItem workItem;

    public Attachment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public WorkItem getWorkItem() {
        return workItem;
    }

    public void setWorkItem(WorkItem workItem) {
        this.workItem = workItem;
    }
}
