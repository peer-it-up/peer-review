package com.telerikacademy.web.peerReview.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "work_item_statuses")
public class Status {
    @Id
    @Column(name = "work_item_status_id")
    private int id;

    @Column(name = "work_item_status")
    private String statusName;

    public Status() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
