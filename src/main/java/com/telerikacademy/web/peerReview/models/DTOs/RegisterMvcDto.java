package com.telerikacademy.web.peerReview.models.DTOs;

import org.springframework.web.multipart.MultipartFile;

public class RegisterMvcDto extends RegisterDto {

    private MultipartFile multipartFile;

    public RegisterMvcDto() {
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
