package com.telerikacademy.web.peerReview.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ResponseWorkItemDto {


    private String comment;

    @Positive(message = "Status Id should be positive number")
    @NotNull(message = "Status can't be empty")
    private int statusId;

    public ResponseWorkItemDto() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
