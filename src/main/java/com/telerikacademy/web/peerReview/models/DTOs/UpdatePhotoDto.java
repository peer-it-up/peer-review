package com.telerikacademy.web.peerReview.models.DTOs;

import org.springframework.web.multipart.MultipartFile;

public class UpdatePhotoDto {

    private MultipartFile photo;

    public UpdatePhotoDto() {
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }
}
