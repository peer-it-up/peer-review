package com.telerikacademy.web.peerReview.models.DTOs;

public class InvitationResoponseDto {
    private int invitationId;
    private boolean accepted;

    public InvitationResoponseDto() {
    }

    public int getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(int invitationId) {
        this.invitationId = invitationId;
    }

    public boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
