package com.telerikacademy.web.peerReview.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class WorkItemDto {

    @Size(min = 10, max = 80, message = "Size of title must be between 10 and 80 symbols")
    @NotNull
    private String title;

    @Size(min = 20, message = "Size must be more than 20 symbols")
    @NotNull
    private String description;


    @Positive(message = "Team Id should be positive number")
    @NotNull(message = "Team can't be empty")
    private int teamId;

    @Positive(message = "Reviewer Id should be positive number")
    @NotNull(message = "Reviewer can't be empty")
    private int reviewerId;

    public WorkItemDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(int reviewerId) {
        this.reviewerId = reviewerId;
    }

}
