package com.telerikacademy.web.peerReview.models;

public enum StatusEnum {
    PENDING(1),
    FOR_REVIEW(2);

    //    TODO proof of concept.
    private int id;

    StatusEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
