package com.telerikacademy.web.peerReview.models.DTOs;

public class NewMembersDto {
    private int memberId;

    public NewMembersDto() {
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
}
