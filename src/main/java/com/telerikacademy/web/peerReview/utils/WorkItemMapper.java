package com.telerikacademy.web.peerReview.utils;

import com.telerikacademy.web.peerReview.models.DTOs.WorkItemDto;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class WorkItemMapper {
    public static final int STATUS_PENDING_ID = 1;
    private final WorkItemRepository workItemRepository;
    private final TeamRepository teamRepository;
    private final UserRepository userRepository;


    @Autowired
    public WorkItemMapper(WorkItemRepository workItemRepository, TeamRepository teamRepository, UserRepository userRepository) {
        this.workItemRepository = workItemRepository;
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    public WorkItem fromDto(WorkItemDto workItemDto, User creator) {
        WorkItem workItem = new WorkItem();
        workItem.setCreator(creator);
        dtoToObject(workItemDto, workItem);
        return workItem;
    }

    public WorkItem fromResponseDto(Optional<String> comment, int statusId, int id) {
        WorkItem workItem = workItemRepository.getById(id);
        workItem.setStatus(workItemRepository.getStatusById(statusId));
        return workItem;
    }

    private void dtoToObject(WorkItemDto workItemDto, WorkItem workItem) {
        workItem.setTitle(workItemDto.getTitle());
        workItem.setDescription(workItemDto.getDescription());
        workItem.setDescription(workItemDto.getDescription());
        workItem.setTeam(teamRepository.getById(workItemDto.getTeamId()));
//        workItem.setFile(workItemDto.getFile());
        workItem.setStatus(workItemRepository.getStatusById(STATUS_PENDING_ID));
        workItem.setReviewer(userRepository.getById(workItemDto.getReviewerId()));

    }

    public WorkItemDto objectToDto(WorkItem workItem) {
        WorkItemDto workItemDto = new WorkItemDto();

        workItemDto.setTitle(workItem.getTitle());
        workItemDto.setDescription(workItem.getDescription());
        workItemDto.setTeamId(workItem.getTeam().getId());
//        workItemDto.setFile(workItem.getFile());
        workItemDto.setReviewerId(workItem.getReviewer().getId());

        return workItemDto;
    }
}