package com.telerikacademy.web.peerReview.utils;

import com.telerikacademy.web.peerReview.models.DTOs.TeamDto;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeamMapper {
    public static final int REGULAR_USER_ROLE_ID = 1;
    private final UserRepository userRepository;
    private final UserService userService;
    private final TeamRepository teamRepository;


    @Autowired
    public TeamMapper(UserRepository userRepository, UserService userService, TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.userService = userService;

        this.teamRepository = teamRepository;
    }

    public Team fromDto(TeamDto teamDto, User user) {
        Team team = new Team();
        team.setOwner(user);
        dtoToObject(teamDto, team);
        return team;
    }

    public Team fromDto(TeamDto teamDto, int id) {
        Team team = teamRepository.getById(id);
        dtoToObject(teamDto, team);
        return team;
    }

    private void dtoToObject(TeamDto teamDto, Team team) {
        team.setName(teamDto.getName());
    }


//    public RegisterDto toDto(User user) {
//        RegisterDto registerDto = new RegisterDto();
//        registerDto.setFirstName(user.getFirstName());
//        registerDto.setLastName(user.getLastName());
//        registerDto.setEmail(user.getEmail());
//        registerDto.setPassword(user.getPassword());
//        registerDto.setPasswordConfirm(user.getPassword());
//        return registerDto;
//    }

//    public TeamDto objectToDto(Team team) {
//        TeamDto teamDto = new TeamDto();
//        teamDto.setName(team.getName());
//
//        teamDto.setMembersId(team.get);
//        return teamDto;
//    }


}
