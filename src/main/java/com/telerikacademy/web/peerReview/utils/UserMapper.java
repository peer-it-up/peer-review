package com.telerikacademy.web.peerReview.utils;


import com.telerikacademy.web.peerReview.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.DTOs.RegisterDto;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;


@Component
public class UserMapper {
    public static final int REGULAR_USER_ROLE_ID = 2;
    public static final String PASSWORD_MISMATCH = "The password did not match the confirmation password";
    public static final String PASSWORD_MUST_HAVE_AT_LEAST_8_CHARACTERS = "Password must have at least 8 characters";
    public static final String PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_SPECIAL_CHARACTER = "Password must contain at least one special character";
    public static final String PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_UPPER_CASE_CHARACTER = "Password must contain at least one upper-case character";
    public static final String PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_LOWER_CASE_CHARACTER = "Password must contain at least one lower-case character";
    public static final String PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_DIGIT = "Password must contain at least one digit";
    private final UserRepository userRepository;


    @Autowired
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        dtoToObject(registerDto, user);
        return user;
    }

    public User fromDto(RegisterDto registerDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(registerDto, user);
        return user;
    }

    private void dtoToObject(RegisterDto registerDto, User user) {

        isValidPassword(registerDto.getPassword());


        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            throw new AuthenticationFailureException(PASSWORD_MISMATCH);
        }

        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(registerDto.getPassword());
        user.setUsername(registerDto.getUsername());
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setPhotoName("DefaultSmileyFace.png");
        user.setRole(userRepository.getRoleById(REGULAR_USER_ROLE_ID));
        user.setActive(true);
    }

    public RegisterDto toDto(User user) {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setFirstName(user.getFirstName());
        registerDto.setLastName(user.getLastName());
        registerDto.setUsername(user.getUsername());
        registerDto.setPhoneNumber(user.getPhoneNumber());
        registerDto.setEmail(user.getEmail());
        registerDto.setPassword(user.getPassword());
        registerDto.setPasswordConfirm(user.getPassword());
        return registerDto;
    }

    public static boolean isValidPassword(String password) {
        String errorMessage = "";
        Pattern specialCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");

        boolean flag = true;

        if (password.length() < 8) {
            flag = false;
            errorMessage = PASSWORD_MUST_HAVE_AT_LEAST_8_CHARACTERS;
        }

        if (!specialCharPatten.matcher(password).find()) {
            flag = false;
            errorMessage = PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_SPECIAL_CHARACTER;
        }

        if (!UpperCasePatten.matcher(password).find()) {
            flag = false;
            errorMessage = PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_UPPER_CASE_CHARACTER;
        }

        if (!lowerCasePatten.matcher(password).find()) {
            flag = false;
            errorMessage = PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_LOWER_CASE_CHARACTER;
        }

        if (!digitCasePatten.matcher(password).find()) {
            flag = false;
            errorMessage = PASSWORD_MUST_CONTAIN_AT_LEAST_ONE_DIGIT;
        }

        if (flag) {
            return flag;
        } else throw new UnauthorizedOperationException(errorMessage);

    }


}
