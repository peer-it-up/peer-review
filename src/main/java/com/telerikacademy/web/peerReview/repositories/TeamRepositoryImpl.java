package com.telerikacademy.web.peerReview.repositories;

import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeamRepositoryImpl extends AbstractCRUDRepository<Team> implements TeamRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TeamRepositoryImpl(SessionFactory sessionFactory) {
        super(Team.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Team getByTeamName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Team> query = session.createQuery("from Team where name = :name", Team.class);
            query.setParameter("name", name);
            List<Team> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Team", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public void createInvitation(Invitation invitation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(invitation);
            session.getTransaction().commit();
        }
    }

    @Override
    public Invitation getInvitationById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Invitation invitation = session.get(Invitation.class, id);
            if (invitation == null) {
                throw new EntityNotFoundException("Invitation", id);
            }
            return invitation;
        }
    }

    @Override
    public void updateInvitation(Invitation invitation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(invitation);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteInvitation(Invitation invitation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(invitation);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getAllTeamMembers(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(
                            "select distinct u from User u" +
                                    " join u.invitations t " +
                                    " where t.team.id = :teamId" +
                                    " and u.isActive = true " +
                                    "and t.accepted = true", User.class)
                    .setParameter("teamId", id)
                    .getResultList();
        }
    }
}
