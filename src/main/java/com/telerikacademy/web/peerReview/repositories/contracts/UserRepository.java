package com.telerikacademy.web.peerReview.repositories.contracts;

import com.telerikacademy.web.peerReview.models.Role;
import com.telerikacademy.web.peerReview.models.User;

import java.util.List;

public interface UserRepository extends BaseReadRepository<User> {
    void create(User user);

    void update(User user);

    void delete(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    Role getRoleById(int id);


    List<User> getAllUsers();

}
