package com.telerikacademy.web.peerReview.repositories.contracts;

import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;

import java.util.List;

public interface TeamRepository extends BaseReadRepository<Team> {

    void create(Team team);

    void update(Team team);

    void delete(int id);

    Team getByTeamName(String teamName);

    void updateInvitation(Invitation invitation);

    void deleteInvitation(Invitation invitation);

    List<User> getAllTeamMembers(int id);

    void createInvitation(Invitation invitation);

    Invitation getInvitationById(int id);
}
