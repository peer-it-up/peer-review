package com.telerikacademy.web.peerReview.repositories;

import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.models.Attachment;
import com.telerikacademy.web.peerReview.models.Comment;
import com.telerikacademy.web.peerReview.models.Status;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class WorkItemRepositoryImpl extends AbstractCRUDRepository<WorkItem> implements WorkItemRepository {
    public static final String ID_STATUS_PENDING = "1";
    public static final String ID_STATUS_ACCEPTED = "4";
    public static final String ID_STATUS_REJECTED = "5";
    private final SessionFactory sessionFactory;


    @Autowired
    public WorkItemRepositoryImpl(SessionFactory sessionFactory) {
        super(WorkItem.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<WorkItem> listPendingWorkItemsOfTeam(int teamId) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery("from WorkItem where team.id = :teamId and status.statusName = 'Pending'", WorkItem.class);
            query.setParameter("teamId", teamId);

            return query.list();
        }
    }

    @Override
    public List<WorkItem> listUserCreatedWorkItems(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery("from WorkItem where creator.id = :userId", WorkItem.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public Attachment getAttachmentById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Attachment attachment = session.get(Attachment.class, id);
            if (attachment == null) {
                throw new EntityNotFoundException("Attachment", id);
            }
            return attachment;
        }
    }

    @Override
    public List<Attachment> getAllAttachments(int workItemId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Attachment> query = session.createQuery("from Attachment where workItem.id = :workItemId ", Attachment.class);
            query.setParameter("workItemId", workItemId);
            return query.list();
        }
    }

    @Override
    public void createAttachment(Attachment attachment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(attachment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateAttachment(Attachment attachment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(attachment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<WorkItem> listPendingItemsForReviewByThisUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery("from WorkItem where reviewer.id = :userId " +
                    "and status.id = " + ID_STATUS_PENDING + " order by id desc", WorkItem.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public List<WorkItem> listItemsForReviewByThisUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery("from WorkItem where reviewer.id = :userId order by id desc", WorkItem.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public Status getStatusById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, id);
            if (status == null) {
                throw new EntityNotFoundException("Status", id);
            }
            return status;
        }
    }

    @Override
    public void createComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Comment> getAllComments(int workItemId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where workItem.id = :workItemId order by timestamp desc ", Comment.class);
            query.setParameter("workItemId", workItemId);
            return query.list();
        }
    }

    @Override
    public List<WorkItem> filter(Optional<String> creatorUsername, Optional<String> reviewerUsername, Optional<Integer> statusId,
                                 Optional<Integer> teamId, Optional<String> title, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from WorkItem ");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();
            if (!creatorUsername.get().equals("")) {
                creatorUsername.ifPresent(value -> {
                    filters.add("creator.username = :creatorUsername");
                    params.put("creatorUsername", value);
                });
            }
            if (!reviewerUsername.get().equals("")) {
                reviewerUsername.ifPresent(value -> {
                    filters.add("reviewer.username = :reviewerUsername");
                    params.put("reviewerUsername", value);
                });
            }
            if (statusId.get() != -1) {
                statusId.ifPresent(value -> {
                    filters.add("status.id = :statusId");
                    params.put("statusId", value);
                });
            }
            if (teamId.get() != -1) {
                teamId.ifPresent(value -> {
                    filters.add("team.id = :teamId");
                    params.put("teamId", value);
                });
            }
            if (!title.get().equals("")) {
                title.ifPresent(value -> {
                    filters.add("title = :title");
                    params.put("title", value);
                });
            }
            if (!filters.isEmpty()) {
                queryString.append("where ")
                        .append(String.join(" and ", filters));
            }
            if (!sort.get().equals("")) {
                sort.ifPresent(value -> queryString.append(generateSortingString(value)));
            }
            Query<WorkItem> query = session.createQuery(queryString.toString(), WorkItem.class);
            query.setProperties(params);
            return query.list();

        }
    }


    private String generateSortingString(String value) {
        StringBuilder result = new StringBuilder(" order by ");
        var params = value.toLowerCase().split("_");
        switch (params[0]) {
            case "id":
                result.append("id ");
                break;
            case "title":
                result.append("title ");
                break;
            case "status":
                result.append("status.statusName ");
                break;
            default:
                return "";
        }
        if (params.length > 1 && params[1].equals("desc")) {
            result.append("desc");
        }
        return result.toString();
    }

    @Override
    public List<WorkItem> listUnresolvedItemsForReviewByThisUserInTeam(int userId, int teamId) {
        try (Session session = sessionFactory.openSession()) {
            Query<WorkItem> query = session.createQuery("from WorkItem where reviewer.id = :userId and team.id = :teamId" +
                    " and status.id != " + ID_STATUS_ACCEPTED +
                    " and status.id != " + ID_STATUS_REJECTED +
                    " order by id desc", WorkItem.class);
            query.setParameter("userId", userId);
            query.setParameter("teamId", teamId);
            return query.list();
        }
    }

    @Override
    public List<Status> getAllStatuses() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status ", Status.class);
            return query.list();
        }
    }
}
