package com.telerikacademy.web.peerReview.repositories.contracts;

import com.telerikacademy.web.peerReview.models.Attachment;
import com.telerikacademy.web.peerReview.models.Comment;
import com.telerikacademy.web.peerReview.models.Status;
import com.telerikacademy.web.peerReview.models.WorkItem;

import java.util.List;
import java.util.Optional;


public interface WorkItemRepository extends BaseReadRepository<WorkItem> {
    void create(WorkItem workItem);

    Attachment getAttachmentById(int id);

    void createAttachment(Attachment attachment);

    void updateAttachment(Attachment attachment);

    void update(WorkItem workItem);

    void delete(int id);

    List<WorkItem> listPendingItemsForReviewByThisUser(int userId);

    List<WorkItem> listItemsForReviewByThisUser(int userId);

    Status getStatusById(int id);

    List<Status> getAllStatuses();

    void createComment(Comment comment);

    List<WorkItem> listPendingWorkItemsOfTeam(int teamId);

    List<WorkItem> listUserCreatedWorkItems(int userId);

    List<WorkItem> filter(Optional<String> creatorUsername, Optional<String> reviewerUsername, Optional<Integer> statusId,
                          Optional<Integer> teamId, Optional<String> title, Optional<String> sort);

    List<WorkItem> listUnresolvedItemsForReviewByThisUserInTeam(int userId, int teamId);

    List<Attachment> getAllAttachments(int workItemId);

    List<Comment> getAllComments(int workItemId);
}
