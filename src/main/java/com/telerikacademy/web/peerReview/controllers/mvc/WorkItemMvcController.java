package com.telerikacademy.web.peerReview.controllers.mvc;

import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.DTOs.ResponseWorkItemDto;
import com.telerikacademy.web.peerReview.models.DTOs.SearchWorkItemDto;
import com.telerikacademy.web.peerReview.models.DTOs.UpdatePhotoDto;
import com.telerikacademy.web.peerReview.models.DTOs.WorkItemFIleMvcDto;
import com.telerikacademy.web.peerReview.models.*;
import com.telerikacademy.web.peerReview.services.contracts.TeamService;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import com.telerikacademy.web.peerReview.utils.WorkItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/work_items")
public class WorkItemMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final WorkItemService workItemService;
    private final TeamService teamService;
    private final UserService userService;
    private final WorkItemMapper workItemMapper;

    @Autowired
    public WorkItemMvcController(AuthenticationHelper authenticationHelper, WorkItemService workItemService, TeamService teamService, UserService userService, WorkItemMapper workItemMapper) {
        this.authenticationHelper = authenticationHelper;
        this.workItemService = workItemService;
        this.teamService = teamService;
        this.userService = userService;
        this.workItemMapper = workItemMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdministrator")
    public boolean populateIsAdministrator(HttpSession session) {
        return authenticationHelper.isAdministrator(session);
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("invitations")
    public List<Invitation> populateInvitations(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getUserInvitations(user);
        }
        return null;
    }

    @ModelAttribute("pendingWorkItems")
    public List<WorkItem> populatePendingWorkItems(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return workItemService.listPendingItemsForReviewByThisUser(user.getId());
        }
        return null;
    }

    @ModelAttribute("allUsers")
    public List<User> populateAllUsers(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getAllUsers(user);
        }
        return null;
    }

    @ModelAttribute("userTeams")
    public List<Team> populateUserTeams(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return teamService.getAll(user);
        }
        return null;
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return workItemService.getAllStatuses();
        }
        return null;
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("id", "id_desc", "title", "title_desc", "status", "status_desc"));
    }

    @GetMapping("/{id}")
    public String showSingleWorkItem(@PathVariable int id, Model model, HttpSession session) {
        if (!authenticationHelper.isAuthenticated(session)) {
            return "redirect:/auth/login";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            WorkItem workItemToShow = workItemService.getById(id);
            model.addAttribute("workItem", workItemToShow);
            model.addAttribute("attachments", workItemService.getAllAttachments(id));
            model.addAttribute("comments", workItemService.getAllComments(id));
            model.addAttribute("responseDto", new ResponseWorkItemDto());
            return "work-item";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}")
    public String reviewWorkItem(@PathVariable int id, @Valid @ModelAttribute("responseDto") ResponseWorkItemDto responseWorkItemDto,
                                 BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (
                AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {

            return "access-denied";
        }
        try {
            WorkItem workItemToUpdate = workItemService.getById(id);
            workItemService.update(user, workItemToUpdate, responseWorkItemDto.getStatusId(), Optional.of(responseWorkItemDto.getComment()));
            return "redirect:/work_items/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (ChangesDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "changes-denied";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showCreateWorkItemPage(Model model, HttpSession session) {
        User user;
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("workItem", new WorkItemFIleMvcDto());
        return "work-item-create";
    }

    @PostMapping("/new")
    public String createWorkItem(@Valid @ModelAttribute("workItem") WorkItemFIleMvcDto workItemDto,
                                 BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        if (errors.hasErrors()) {
            return "work-item-create";
        }
        try {
            WorkItem workItem = workItemMapper.fromDto(workItemDto, user);
            int id = workItemService.create(workItem, Optional.of(workItemDto.getMultipartFile()));
            model.addAttribute("workItem", workItem);

            return (String.format("redirect:http://localhost:8080/work_items/%s", id));
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("{id}/attach")
    public String showAttachToWorkItem(Model model, @PathVariable String id) {

        model.addAttribute("attachment", new UpdatePhotoDto());
        return "add-attachment";
    }

    @PostMapping("{id}/attach")
    public String attachToWorkItem(@PathVariable int id, @ModelAttribute("attachment") UpdatePhotoDto updatePhotoDto,
                                   BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        if (errors.hasErrors()) {
            return "work-item-create";
        }
        try {
            WorkItem workItem = workItemService.getById(id);
            workItemService.attach(user, id, updatePhotoDto.getPhoto());
            return (String.format("redirect:http://localhost:8080/work_items/%s", id));
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/created")
    public String showCreatedWorkItems(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("workItems", workItemService.listUserCreatedWorkItems(user.getId()));
        model.addAttribute("addressee", "Reviewer");
        model.addAttribute("searchWorkItemDto", new SearchWorkItemDto());
        return "work-items";
    }

    @PostMapping("/created")
    public String filterCreated(@ModelAttribute("searchWorkItemDto") SearchWorkItemDto searchWorkItemDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        List<WorkItem> workItems = workItemService.filter(user, Optional.of(user.getUsername()),
                Optional.of(searchWorkItemDto.getReviewerUsername()),
                Optional.of(searchWorkItemDto.getStatusId()),
                Optional.of(searchWorkItemDto.getTeamId()),
                Optional.of(searchWorkItemDto.getTitle()),
                Optional.of(searchWorkItemDto.getSort()));
        model.addAttribute("workItems", workItems);
        model.addAttribute("addressee", "Reviewer");
        return "work-items";
    }

    @GetMapping("/for_review")
    public String showWorkItemsForReview(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("workItems", workItemService.listItemsForReviewByThisUser(user.getId()));
        model.addAttribute("addressee", "Creator");
        model.addAttribute("searchWorkItemDto", new SearchWorkItemDto());
        return "work-items";
    }

    @PostMapping("/for_review")
    public String filterForReview(@ModelAttribute("searchWorkItemDto") SearchWorkItemDto searchWorkItemDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        List<WorkItem> workItems = workItemService.filter(user, Optional.of(searchWorkItemDto.getCreatorUsername()),
                Optional.of(user.getUsername()),
                Optional.of(searchWorkItemDto.getStatusId()),
                Optional.of(searchWorkItemDto.getTeamId()),
                Optional.of(searchWorkItemDto.getTitle()),
                Optional.of(searchWorkItemDto.getSort()));
        model.addAttribute("workItems", workItems);
        model.addAttribute("addressee", "Creator");
        return "work-items";
    }

    @GetMapping("/pending")
    public String showPendingWorkItemsForReview(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("workItems", workItemService.listPendingItemsForReviewByThisUser(user.getId()));
        model.addAttribute("addressee", "Creator");
        model.addAttribute("searchWorkItemDto", new SearchWorkItemDto());
        return "work-items";
    }

    @PostMapping("/pending")
    public String filterPending(@ModelAttribute("searchWorkItemDto") SearchWorkItemDto searchWorkItemDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        List<WorkItem> workItems = workItemService.filter(user, Optional.of(user.getUsername()),
                Optional.of(searchWorkItemDto.getReviewerUsername()),
                Optional.of(searchWorkItemDto.getStatusId()),
                Optional.of(searchWorkItemDto.getTeamId()),
                Optional.of(searchWorkItemDto.getTitle()),
                Optional.of(searchWorkItemDto.getSort()));
        model.addAttribute("workItems", workItems);
        model.addAttribute("addressee", "Creator");
        return "work-items";
    }

    @GetMapping("/team/{id}")
    public String showPendingWorkItemsOfTeam(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            Team team = teamService.getById(id);
            model.addAttribute("workItems", workItemService.listPendingWorkItemsOfTeam(user, team));
            model.addAttribute("addressee", "Creator");
            model.addAttribute("searchWorkItemDto", new SearchWorkItemDto());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "work-items";
    }

    @PostMapping("/team/{id}")
    public String filterPendingWorkItemOfTeam(@ModelAttribute("searchWorkItemDto") SearchWorkItemDto searchWorkItemDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        List<WorkItem> workItems = workItemService.filter(user, Optional.of(searchWorkItemDto.getCreatorUsername()),
                Optional.of(searchWorkItemDto.getReviewerUsername()),
                Optional.of(searchWorkItemDto.getStatusId()),
                Optional.of(searchWorkItemDto.getTeamId()),
                Optional.of(searchWorkItemDto.getTitle()),
                Optional.of(searchWorkItemDto.getSort()));
        model.addAttribute("workItems", workItems);
        model.addAttribute("addressee", "Creator");
        return "work-items";
    }
}
