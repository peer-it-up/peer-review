package com.telerikacademy.web.peerReview.controllers.mvc;


import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final WorkItemService workItemService;

    public HomeMvcController(AuthenticationHelper authenticationHelper, UserService userService, UserRepository userRepository, WorkItemService workItemService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.workItemService = workItemService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdministrator")
    public boolean populateIsAdministrator(HttpSession session) {
        return authenticationHelper.isAdministrator(session);
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("invitations")
    public List<Invitation> populateInvitations(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getUserInvitations(user);
        }
        return null;
    }

    @ModelAttribute("pendingWorkItems")
    public List<WorkItem> populatePendingWorkItems(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return workItemService.listPendingItemsForReviewByThisUser(user.getId());
        }
        return null;
    }

    @GetMapping
    public String showHomePage(HttpSession session, Model model) {
        return "index";
    }
}
