package com.telerikacademy.web.peerReview.controllers.rest;

import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.Attachment;
import com.telerikacademy.web.peerReview.models.DTOs.WorkItemDto;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import com.telerikacademy.web.peerReview.services.contracts.TeamService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import com.telerikacademy.web.peerReview.utils.WorkItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/work_items")
public class WorkItemController {
    public static final String WORK_ITEM_DELETED_SUCCESSFULLY = "WorkItem was deleted successfully!";
    private final WorkItemService workItemService;
    private final TeamService teamService;
    private final WorkItemMapper workItemMapper;
    private final AuthenticationHelper authenticationHelper;
    private final FileStore fileStore;

    @Autowired
    public WorkItemController(WorkItemService service, TeamService teamService, WorkItemMapper workItemMapper, AuthenticationHelper authenticationHelper, FileStore fileStore) {
        this.teamService = teamService;
        this.workItemService = service;
        this.workItemMapper = workItemMapper;
        this.authenticationHelper = authenticationHelper;
        this.fileStore = fileStore;
    }

    @GetMapping
    public List<WorkItem> getAll() {
        return workItemService.getAll();
    }

    @GetMapping("/pending/team/{id}")
    public List<WorkItem> listPendingWorkItemsOfTeam(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamService.getById(id);
            return workItemService.listPendingWorkItemsOfTeam(user, team);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/creator")
    public List<WorkItem> listUserCreatedWorkItems(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.listUserCreatedWorkItems(user.getId());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/reviewer")
    public List<WorkItem> listItemsForReviewByThisUser(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.listItemsForReviewByThisUser(user.getId());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/pending_reviewer")
    public List<WorkItem> listPendingItemsForReviewByThisUser(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.listPendingItemsForReviewByThisUser(user.getId());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public WorkItem getById(@PathVariable int id) {
        try {
            return workItemService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public WorkItem create(@RequestHeader HttpHeaders headers,
                           @Valid @RequestPart(name = "dto") WorkItemDto workItemDto,
                           @RequestPart(required = false) Optional<MultipartFile> multipartFile) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemMapper.fromDto(workItemDto, creator);
            try {
                int workItemId = workItemService.create(workItem, multipartFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return workItem;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ChangesDeniedException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public WorkItem review(@RequestHeader HttpHeaders headers, @PathVariable int id,
                           @RequestParam(required = false) Optional<String> comment,
                           @RequestParam() int statusId
    ) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            WorkItem workItemToUpdate = workItemService.getById(id);
            workItemService.update(user, workItemToUpdate, statusId, comment);
            return workItemToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ChangesDeniedException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            workItemService.delete(user, id);
            return WORK_ITEM_DELETED_SUCCESSFULLY;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ChangesDeniedException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<WorkItem> filter(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<String> creatorUsername,
                                 @RequestParam(required = false) Optional<String> reviewerUsername,
                                 @RequestParam(required = false) Optional<Integer> statusId,
                                 @RequestParam(required = false) Optional<Integer> teamId,
                                 @RequestParam(required = false) Optional<String> title,
                                 @RequestParam(required = false) Optional<String> sort) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return workItemService.filter(user, creatorUsername, reviewerUsername, statusId, teamId, title, sort);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/attachment")
    public String attachFile(@RequestHeader HttpHeaders headers, @PathVariable int id,
                             @RequestBody MultipartFile multipartFile) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            workItemService.attach(user, id, multipartFile);
            return "File attached successfully!";
        } catch (UnauthorizedOperationException | IOException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{workItemId}/file")
    public ResponseEntity<ByteArrayResource> downloadContent(@RequestHeader HttpHeaders headers, @PathVariable int workItemId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemService.getById(workItemId);
            byte[] data = workItemService.download(user, workItem);
            return generateResponse(workItem.getFile(), data);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("/{attachmentId}/attachment")
    public ResponseEntity<ByteArrayResource> downloadAttachment(@RequestHeader HttpHeaders headers, @PathVariable int attachmentId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Attachment attachment = workItemService.getAttachmentById(attachmentId);
            byte[] data = workItemService.downloadAttachment(user, attachment);
            return generateResponse(attachment.getAttachmentName(), data);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    public ResponseEntity<ByteArrayResource> generateResponse(String fileName, byte[] data) {
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .contentType(contentType(fileName))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + fileName + "\"")
                .body(resource);
    }

    private MediaType contentType(String filename) {
        String[] fileArrSplit = filename.split("\\.");
        String fileExtension = fileArrSplit[fileArrSplit.length - 1];
        switch (fileExtension) {
            case "txt":
                return MediaType.TEXT_PLAIN;
            case "png":
                return MediaType.IMAGE_PNG;
            case "jpg":
                return MediaType.IMAGE_JPEG;
            case "gif":
                return MediaType.IMAGE_GIF;
            default:
                return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}
