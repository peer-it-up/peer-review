package com.telerikacademy.web.peerReview.controllers.mvc;


import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.exceptions.*;
import com.telerikacademy.web.peerReview.models.DTOs.InvitationResoponseDto;
import com.telerikacademy.web.peerReview.models.DTOs.RegisterDto;
import com.telerikacademy.web.peerReview.models.DTOs.UpdatePhotoDto;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import com.telerikacademy.web.peerReview.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final WorkItemService workItemService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService, WorkItemService workItemService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.workItemService = workItemService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdministrator")
    public boolean populateIsAdministrator(HttpSession session) {
        return authenticationHelper.isAdministrator(session);
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("invitations")
    public List<Invitation> populateInvitations(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getUserInvitations(user);
        }
        return null;
    }

    @ModelAttribute("pendingWorkItems")
    public List<WorkItem> populatePendingWorkItems(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return workItemService.listPendingItemsForReviewByThisUser(user.getId());
        }
        return null;
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        if (!authenticationHelper.isAuthenticated(session)) {
            return "redirect:/auth/login";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            User userToShow = userService.getById(id);
            model.addAttribute("user", userToShow);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!authenticationHelper.isAuthenticated(session)) {
                return "login";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            User userToUpdate = user;
            RegisterDto registerDto = userMapper.toDto(userToUpdate);
            model.addAttribute("userUpdate", registerDto);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id, @Valid @ModelAttribute("userUpdate") RegisterDto dto,
                             BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        if (errors.hasErrors()) {
            return "user-update";
        }
        try {
            User userToUpdate = userMapper.fromDto(dto, user.getId());
            userToUpdate.setPhotoName(user.getPhotoName());
            userService.update(user, userToUpdate);
            return "redirect:/users/{id}";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_email", e.getMessage());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/photo")
    public String showUpdatePhotoPage(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!authenticationHelper.isAuthenticated(session)) {
                return "access-denied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            User userToUpdate = user;
            RegisterDto registerDto = userMapper.toDto(userToUpdate);
            model.addAttribute("photo", new UpdatePhotoDto());

            return "update_photo";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/photo")
    public String handleUpdaetPhotoPage(@Valid @ModelAttribute("photo") UpdatePhotoDto updatePhotoDto,
                                        BindingResult bindingResult, Model model,
                                        HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (!authenticationHelper.isAuthenticated(session)) {
                return "access-denied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        if (bindingResult.hasErrors()) {
            return "update_photo";
        }
        try {
            userService.updatePhoto(user, updatePhotoDto.getPhoto());
            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "update_photo";
        } catch (IOException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/teams")
    public String listUserTeams(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("teams", userService.getUserTeams(user));
        int teamId = 0;
        model.addAttribute("teamId", teamId);

        return "teams";
    }

    @PostMapping("/teams")
    public String leaveTeam(@ModelAttribute("teamId") int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            userService.leaveTeam(user, id);
            return ("redirect:http://localhost:8080/users/teams");
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (ChangesDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "changes-denied";
        }
    }

    @GetMapping("/invitations")
    public String listUserTeamInvitations(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("invitations", userService.getUserInvitations(user));
        model.addAttribute("invitationResponse", new InvitationResoponseDto());
        return "invitations";
    }

    @PostMapping("/invitations")
    public String responseToTeamInvitation(@ModelAttribute("invitationResponse") InvitationResoponseDto invitationResponse, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            model.addAttribute("invitations", userService.getUserInvitations(user));
            userService.respondToInvitation(user, invitationResponse.getInvitationId(), invitationResponse.getAccepted());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        return ("redirect:http://localhost:8080/users/invitations");
    }
}
