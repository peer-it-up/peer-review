package com.telerikacademy.web.peerReview.controllers.mvc;

import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.DTOs.NewMembersDto;
import com.telerikacademy.web.peerReview.models.DTOs.TeamDto;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.services.contracts.TeamService;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import com.telerikacademy.web.peerReview.utils.TeamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/teams")
public class TeamMvcController {

    public static final String TEAM_CREATED_SUCCESSFULLY = "Team created successfully";
    private final AuthenticationHelper authenticationHelper;
    private final TeamService teamService;
    private final WorkItemService workItemService;
    private final TeamMapper teamMapper;
    private final UserService userService;

    @Autowired
    public TeamMvcController(AuthenticationHelper authenticationHelper, TeamService teamService, WorkItemService workItemService, TeamMapper teamMapper, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.teamService = teamService;
        this.workItemService = workItemService;
        this.teamMapper = teamMapper;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdministrator")
    public boolean populateIsAdministrator(HttpSession session) {
        return authenticationHelper.isAdministrator(session);
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("invitations")
    public List<Invitation> populateInvitations(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getUserInvitations(user);
        }
        return null;
    }

    @ModelAttribute("pendingWorkItems")
    public List<WorkItem> populatePendingWorkItems(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return workItemService.listPendingItemsForReviewByThisUser(user.getId());
        }
        return null;
    }

    @ModelAttribute("allUsers")
    public List<User> populateAllUsers(HttpSession session) {
        if (authenticationHelper.isAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return userService.getAllUsers(user);
        }
        return null;
    }

    @GetMapping
    public List<Team> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/members/{id}")
    public List<User> getTeamMembers(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getAllTeamMembers(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/new")
    public String create(Model model, HttpSession session) {
        User user;
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("team", new TeamDto());
        return "team-create";
    }

    @PostMapping("/new")
    public String create(@Valid @ModelAttribute("team") TeamDto teamDto,
                         BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

        if (errors.hasErrors()) {
            return "team-create";
        }
        try {
            Team team = teamMapper.fromDto(teamDto, user);
            teamService.create(team, teamDto.getMembersId());
            int id = teamService.getByTeamName(team.getName()).getId();
            model.addAttribute("team", team);
            return (String.format("redirect:http://localhost:8080/teams/%s/members", id));
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/members")
    public String listTeamMembers(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("members", teamService.getAllTeamMembers(user, id));
        model.addAttribute("newMembers", new NewMembersDto());
        return "team-members";
    }

    @PostMapping("/{id}/members")
    public String addNewMembersToTeam(@PathVariable int id, @ModelAttribute("newMembers") NewMembersDto newMembers, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        try {
            teamService.addNewMembersToTeam(id, user, newMembers.getMemberId());
            return String.format("redirect:http://localhost:8080/teams/%s/members", id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
