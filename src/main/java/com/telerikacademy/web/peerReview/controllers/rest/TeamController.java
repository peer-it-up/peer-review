package com.telerikacademy.web.peerReview.controllers.rest;

import com.telerikacademy.web.peerReview.controllers.authentication.AuthenticationHelper;
import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.DuplicateEntityException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.DTOs.TeamDto;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.services.contracts.TeamService;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.utils.TeamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

    public static final String TEAM_CREATED = "Team Created";
    public static final String TEAM_DELETED = "Team Deleted";
    public static final String MEMBERS_ADDED = "Members added successfully";
    public static final String USER_LEFT_TEAM = "User left the team";
    public static final String USER_REMOVED = "User removed from team";
    private final TeamService teamService;
    private final TeamMapper teamMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public TeamController(TeamService teamService, TeamMapper teamMapper, AuthenticationHelper authenticationHelper, UserService userService) {
        this.teamService = teamService;
        this.teamMapper = teamMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @GetMapping
    public List<Team> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("members/{id}")
    public List<User> getTeamMembers(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return teamService.getAllTeamMembers(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public String create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TeamDto teamDto) {
        try {
            User creator = authenticationHelper.tryGetUser(headers);
            Team team = teamMapper.fromDto(teamDto, creator);
            teamService.create(team, teamDto.getMembersId());
            return TEAM_CREATED;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            teamService.delete(user, id);
            return TEAM_DELETED;
        } catch (ChangesDeniedException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/members")
    public String addUsersToTeam(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody List<Integer> usersId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            teamService.addNewMembersToTeam(id, user, usersId);
            return MEMBERS_ADDED;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("{id}/member")
    public String leaveTeam(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.leaveTeam(user, id);
            return USER_LEFT_TEAM;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("owner/{id}/member")
    public String removeUserFromTeam(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody int memberId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.removeMemberFromTeam(user, memberId, id);
            return USER_REMOVED;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ChangesDeniedException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        }
    }
}

