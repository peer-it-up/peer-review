package com.telerikacademy.web.peerReview.services.contracts;

import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;

import java.util.List;

public interface TeamService {
    List<Team> getAll(User user);

    List<User> getAllTeamMembers(User user, int teamId);

    Team getById(int id);

    Team getByTeamName(String name);

    void create(Team team, List<Integer> membersId);

    void addNewMembersToTeam(int teamId, User user, int memberId);

    void update(User owner, Team updatesTeam);

    void delete(User user, int id);

    void addNewMembersToTeam(int id, User user, List<Integer> usersId);
}
