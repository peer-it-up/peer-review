package com.telerikacademy.web.peerReview.services;


import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.DuplicateEntityException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.services.contracts.TeamService;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {
    public static final String ONLY_OWNER_ADMINISTRATOR_AND_MEMBERS_MAY_ADD_MEMBERS = "You haven't got permission for this operation.";
    public static final String ONLY_OWNER_MAY_DELETE_TEAM = "You can't delete this team, because you are not the owner!";
    public static final String ONLY_THE_OWNER_MAY_UPDATE_THE_TEAM = "Only the owner may update the team";
    public static final String ADMINISTRATORS_HAVE_ACCESS_TO_THIS_INFORMATION = "Only administrators have access to this information";
    public static final String ADMINISTRATORS_OR_TEAM_MEMBERS_HAVE_ACCESS = "Only administrators, team's owner and team members have access to this information";
    public static final int ADMINISTRATOR_ROLE_ID = 1;


    private final TeamRepository teamRepository;
    private final UserService userService;
    private final UserRepository userRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, UserService userService, UserRepository userRepository) {
        this.teamRepository = teamRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @Override
    public List<Team> getAll(User user) {
        return teamRepository.getAll();
    }

    @Override
    public List<User> getAllTeamMembers(User user, int teamId) {
        Team team = getById(teamId);
        return teamRepository.getAllTeamMembers(teamId);
    }

    @Override
    public Team getById(int id) {
        return teamRepository.getById(id);
    }

    @Override
    public Team getByTeamName(String name) {
        return teamRepository.getByTeamName(name);
    }

    @Override
    public void create(Team team, List<Integer> membersId) {
        boolean duplicateExist = true;
        try {
            teamRepository.getByTeamName(team.getName());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityException("Team", "name", team.getName());
        }
        teamRepository.create(team);
        addMembersToTeam(membersId, team);
        User theUserOwner = userService.getById(team.getOwner().getId());
        List<Invitation> list = theUserOwner.getInvitations();
        int size = list.size();
        int invitationIdToAccept = -1;
        for (int i = 0; i < size; i++) {
            if (list.get(i).getTeam().getName().equals(team.getName())) {
                invitationIdToAccept = list.get(i).getId();
                break;
            }
        }
        userService.respondToInvitation(team.getOwner(), invitationIdToAccept, true);
    }

    @Override
    public void addNewMembersToTeam(int id, User user, List<Integer> usersId) {
        Team team = getById(id);

        if (!userService.userIsMemberOfTeam(team, user) && team.getOwner().getId() != user.getId() && !user.isAdministrator()) {
            throw new UnauthorizedOperationException(ONLY_OWNER_ADMINISTRATOR_AND_MEMBERS_MAY_ADD_MEMBERS);
        }
        addMembersToTeam(usersId, team);
    }

    @Override
    public void addNewMembersToTeam(int teamId, User user, int memberId) {
        Team team = getById(teamId);

        if (!userService.userIsMemberOfTeam(team, user) && team.getOwner().getId() != user.getId() && !user.isAdministrator()) {
            throw new UnauthorizedOperationException(ONLY_OWNER_ADMINISTRATOR_AND_MEMBERS_MAY_ADD_MEMBERS);
        }
        User member = userService.getById(memberId);
        Invitation invitation = new Invitation();
        invitation.setMember(member);
        invitation.setTeam(team);
        member.addInvitationToSet(invitation);
        teamRepository.createInvitation(invitation);
        userRepository.update(user);
    }

    void addMembersToTeam(List<Integer> membersId, Team team) {
        for (Integer id : membersId) {
            User user = userService.getById(id);
            Invitation invitation = new Invitation();
            invitation.setTeam(team);
            invitation.setMember(user);
            user.addInvitationToSet(invitation);
            teamRepository.createInvitation(invitation);
            userRepository.update(user);
        }
    }

    @Override
    public void update(User owner, Team updatedTeam) {
        if (updatedTeam.getOwner().getId() != owner.getId() && owner.getRole().getId() != ADMINISTRATOR_ROLE_ID) {
            throw new UnauthorizedOperationException(ONLY_THE_OWNER_MAY_UPDATE_THE_TEAM);
        }
        teamRepository.update(updatedTeam);
    }

    @Override
    public void delete(User user, int id) {
        Team team = getById(id);
        if (!userService.userIsOwnerOfTeam(team, user)) {
            throw new ChangesDeniedException(ONLY_OWNER_MAY_DELETE_TEAM);
        }
        teamRepository.delete(id);
    }
}
