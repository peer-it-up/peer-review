package com.telerikacademy.web.peerReview.services.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStore {


    void uploadFile(MultipartFile multipartFile, String key) throws IOException;

    byte[] download(String key);
}
