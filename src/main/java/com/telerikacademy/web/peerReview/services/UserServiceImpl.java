package com.telerikacademy.web.peerReview.services;

import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.DuplicateEntityException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.repositories.contracts.TeamRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.UserRepository;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.telerikacademy.web.peerReview.services.FileStoreImpl.keyGenerator;


@Service
public class UserServiceImpl implements UserService {

    public static final int ADMINISTRATOR_ROLE_ID = 1;
    public static final String ONLY_ADMINISTRATORS_CAN_VIEW_AL_USERS = "Only Administrators can view al users";
    public static final String PHOTO_MUST_BE_SPECIFIC_FILE_TYPE = "User photo can only be gif, ief, jpeg, jpeg or png";
    public static final String FILE_TYPE_GIF = "image/gif";
    public static final String FILE_TYPE_IEF = "image/ief";
    public static final String FILE_TYPE_JPEG = "image/jpeg";
    public static final String FILE_TYPE_PNG = "image/png";
    public static final String CANT_UPDATE_ANOTHER_USER = "Operation denied! You are trying to update other user's profile";
    public static final String CANT_UPDATE_ANOTHERS_INVITATION = "You are trying to update other's invitation!";
    public static final String CANT_LEAVE_IF_WORK_NOT_DONE = "You can't leave this team until you finish all your tasks.";
    public static final String ONLY_OWNER_MAY_REMOVE_MEMBER = "Only the owner may remove other members from the team";
    public static final String CANT_REMOVE_IF_TASKS_NOT_FINISHED = "You can't remove this member from the team until they finish all their tasks.";


    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final WorkItemRepository workItemRepository;
    private final FileStore fileStore;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, TeamRepository teamRepository, WorkItemRepository workItemRepository, FileStore fileStore) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
        this.workItemRepository = workItemRepository;
        this.fileStore = fileStore;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }


    @Override
    public List<User> getAllUsers(User user) {
        return userRepository.getAllUsers();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(User user, Optional<MultipartFile> userPhoto) {
        boolean duplicateExist = true;
        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        User encodedPasswordUser = encodeThePassword(user);
        if (!userPhoto.get().getOriginalFilename().equals("")) {
            userPhoto.ifPresent(v -> {
                if (fileIsPhoto(userPhoto.get())) {
                    throw new UnsupportedOperationException(PHOTO_MUST_BE_SPECIFIC_FILE_TYPE);
                }
            });
        }
        userRepository.create(encodedPasswordUser);
        userPhoto.ifPresent(v -> {
            if (userPhoto.get().isEmpty()) {
                return;
            }
            try {
                String fileKey = keyGenerator(userPhoto.get().getOriginalFilename(), encodedPasswordUser, encodedPasswordUser.getId());
                fileStore.uploadFile(userPhoto.get(), fileKey);
                encodedPasswordUser.setPhotoName(fileKey);
                userRepository.update(encodedPasswordUser);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    boolean fileIsPhoto(MultipartFile multipartFile) {
        Tika tika = new Tika();
        boolean isPhoto = false;
        try {
            String detectedType = tika.detect(multipartFile.getBytes());
            if (!detectedType.contains(FILE_TYPE_GIF) &&
                    !detectedType.contains(FILE_TYPE_IEF) &&
                    !detectedType.contains(FILE_TYPE_JPEG) &&
                    !detectedType.contains(FILE_TYPE_PNG)
            ) {
                isPhoto = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isPhoto;
    }

    private User encodeThePassword(User user) {
        String encodedPassword = this.passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        return user;
    }

    @Override
    public void update(User user, User userToUpdate) {
        if (user.getId() != userToUpdate.getId()) {
            throw new UnauthorizedOperationException(CANT_UPDATE_ANOTHER_USER);
        }
        boolean duplicateMailExist = true;
        boolean duplicateUsernameExist = true;

        try {
            User existingUserByEmail = userRepository.getByEmail(userToUpdate.getEmail());
            if (existingUserByEmail.getId() == userToUpdate.getId()) {
                duplicateMailExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateMailExist = false;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

        try {
            User existingUserByUsername = userRepository.getByUsername(userToUpdate.getUsername());
            if (existingUserByUsername.getId() == userToUpdate.getId()) {
                duplicateUsernameExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateUsernameExist = false;
        }

        if (duplicateMailExist) {
            throw new DuplicateEntityException("User", "eMail", userToUpdate.getEmail());
        }

        if (duplicateUsernameExist) {
            throw new DuplicateEntityException("User", "username", userToUpdate.getUsername());
        }
        User encodedPasswordUser = encodeThePassword(userToUpdate);
        userRepository.update(encodedPasswordUser);
    }

    @Override
    public void delete(User user, int id) {
        if (user.getId() != id) {
            throw new UnauthorizedOperationException(CANT_UPDATE_ANOTHER_USER);
        }
        user.setActive(false);
        userRepository.update(user);
    }

    @Override
    public List<Invitation> getUserInvitations(User user) {
        return user.getInvitations()
                .stream()
                .filter(a -> !a.getAccepted())
                .collect(Collectors.toList());
    }

    public boolean userIsMemberOfTeam(Team team, User user) {
        Boolean n = user.getInvitations().stream().anyMatch(i ->
                i.getTeam().getId() == team.getId() &&
                        i.getMember().getId() == user.getId() && i.getAccepted());
        return user.getInvitations().stream().anyMatch(i ->
                i.getTeam().getId() == team.getId()
                        && i.getMember().getId() == user.getId() && i.getAccepted());
    }

    public boolean userIsReviewerOfWorkItem(WorkItem workItem, User user) {
        return workItem.getReviewer().getId() == user.getId();
    }

    public boolean userIsOwnerOfTeam(Team team, User user) {
        return team.getOwner().getId() == user.getId();
    }

    @Override
    public void respondToInvitation(User user, int id, boolean accepted) {
        Invitation invitation = teamRepository.getInvitationById(id);
        if (invitation.getMember().getId() != user.getId()) {
            throw new UnauthorizedOperationException(CANT_UPDATE_ANOTHERS_INVITATION);
        }
        if (!accepted) {
            teamRepository.deleteInvitation(invitation);
            return;
        }

        invitation.setAccepted(accepted);
        teamRepository.updateInvitation(invitation);
    }

    @Override
    public void leaveTeam(User user, int teamId) {
        Invitation invitationToDelete = user.getInvitations()
                .stream()
                .filter(Invitation::getAccepted)
                .filter(invitation -> invitation.getTeam().getId() == teamId).findFirst().orElseThrow(() -> new EntityNotFoundException("Team", teamId));


        if (workItemRepository.listUnresolvedItemsForReviewByThisUserInTeam(user.getId(), teamId).size() != 0) {
            throw new ChangesDeniedException(CANT_LEAVE_IF_WORK_NOT_DONE);
        }
        teamRepository.deleteInvitation(invitationToDelete);
    }

    @Override
    public void removeMemberFromTeam(User user, int memberId, int teamId) {
        User member = getById(memberId);
        Team team = teamRepository.getById(teamId);
        if (!userIsOwnerOfTeam(team, user)) {
            throw new ChangesDeniedException(ONLY_OWNER_MAY_REMOVE_MEMBER);
        }
        Invitation invitationToDelete = member.getInvitations()
                .stream()
                .filter(Invitation::getAccepted)
                .filter(invitation -> invitation.getTeam().getId() == teamId).findFirst().orElseThrow(() -> new EntityNotFoundException("Team", teamId));
        if (workItemRepository.listUnresolvedItemsForReviewByThisUserInTeam(member.getId(), teamId).size() != 0) {
            throw new ChangesDeniedException(CANT_REMOVE_IF_TASKS_NOT_FINISHED);
        }

        teamRepository.deleteInvitation(invitationToDelete);
    }

    @Override
    public List<Team> getUserTeams(User user) {
        return user.getInvitations()
                .stream()
                .filter(Invitation::getAccepted)
                .map(Invitation::getTeam)
                .collect(Collectors.toList());
    }

    @Override
    public void updatePhoto(User user, MultipartFile userPhoto) throws IOException {
        if (fileIsPhoto(userPhoto)) {
            throw new UnsupportedOperationException(PHOTO_MUST_BE_SPECIFIC_FILE_TYPE);
        }
        String fileKey = keyGenerator(userPhoto.getOriginalFilename(), user, user.getId());
        fileStore.uploadFile(userPhoto, fileKey);
        user.setPhotoName(fileKey);
        userRepository.update(user);
    }
}