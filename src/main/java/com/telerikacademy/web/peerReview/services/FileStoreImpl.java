package com.telerikacademy.web.peerReview.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.telerikacademy.web.peerReview.models.Attachment;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static java.lang.String.format;

@Service
public class FileStoreImpl implements FileStore {

    public static final String FAILED_TO_DOWNLOAD_THE_FILE = "Failed to download the file";
    private final AmazonS3 amazonS3;
    private String bucketName;
    ObjectMetadata metadata;

    @Autowired
    public FileStoreImpl(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
        this.bucketName = "peer-it-up";
        this.metadata = new ObjectMetadata();
    }

    @Override
    public void uploadFile(MultipartFile multipartFile, String key) throws IOException {
        amazonS3.putObject(bucketName, key, multipartFile.getInputStream(), metadata);
    }

    public static <T> String keyGenerator(String fileName, T object, int objectId) {
        String key = "";
        if (WorkItem.class.equals(object.getClass())) {
            key = format("W%d_%s", objectId, fileName);
        } else if (Attachment.class.equals(object.getClass())) {
            Attachment attachment = (Attachment) object;
            int workItemId = attachment.getWorkItem().getId();
            key = format("W%d_A%d_%s", workItemId, objectId, fileName);
        } else if (User.class.equals(object.getClass())) {
            key = format("UP%d_%s", objectId, fileName);
        }
        return key;
    }

    public byte[] download(String key) {
        try {
            S3Object object = amazonS3.getObject(bucketName, key);
            S3ObjectInputStream objectContent = object.getObjectContent();
            return IOUtils.toByteArray(objectContent);
        } catch (AmazonServiceException | IOException e) {
            throw new IllegalStateException(FAILED_TO_DOWNLOAD_THE_FILE, e);
        }
    }
}
