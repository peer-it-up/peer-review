package com.telerikacademy.web.peerReview.services.contracts;

import com.telerikacademy.web.peerReview.models.Invitation;
import com.telerikacademy.web.peerReview.models.Team;
import com.telerikacademy.web.peerReview.models.User;
import com.telerikacademy.web.peerReview.models.WorkItem;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAllUsers(User user);

    User getById(int id);

    User getByUsername(String username);

    void create(User user, Optional<MultipartFile> userPhoto);

    void update(User user, User userToUpdate);

    void delete(User self, int id);

    boolean userIsMemberOfTeam(Team team, User user);

    boolean userIsReviewerOfWorkItem(WorkItem workItem, User user);

    boolean userIsOwnerOfTeam(Team team, User user);

    List<Invitation> getUserInvitations(User user);

    void respondToInvitation(User user, int id, boolean accepted);

    void leaveTeam(User user, int teamId);

    void removeMemberFromTeam(User user, int memberId, int teamId);

    List<Team> getUserTeams(User user);

    void updatePhoto(User user, MultipartFile userPhoto) throws IOException;
}
