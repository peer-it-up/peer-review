package com.telerikacademy.web.peerReview.services.contracts;

import com.telerikacademy.web.peerReview.models.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface WorkItemService {

    List<WorkItem> getAll();

    byte[] downloadAttachment(User user, Attachment attachment);

    List<WorkItem> listItemsForReviewByThisUser(int userId);

    List<WorkItem> listUserCreatedWorkItems(int userId);

    List<WorkItem> listPendingItemsForReviewByThisUser(int userId);

    List<WorkItem> listPendingWorkItemsOfTeam(User user, Team team);

    WorkItem getById(int id);

    int create(WorkItem workItem, Optional<MultipartFile> multipartFile) throws IOException;

    Attachment getAttachmentById(int id);

    List<Attachment> getAllAttachments(int workItemId);

    void attach(User user, int workItemId, MultipartFile multipartFile) throws IOException;

    void update(User user, WorkItem workItemToUpdate, int statusId, Optional<String> comment);

    void delete(User user, int id);

    List<WorkItem> filter(User user, Optional<String> creatorUsername, Optional<String> reviewerUsername, Optional<Integer> statusId,
                          Optional<Integer> teamId, Optional<String> title, Optional<String> sort);

    byte[] download(User user, WorkItem workItem);

    List<Comment> getAllComments(int workItemId);

    List<Status> getAllStatuses();

}
