package com.telerikacademy.web.peerReview.services;

import com.telerikacademy.web.peerReview.exceptions.ChangesDeniedException;
import com.telerikacademy.web.peerReview.exceptions.EntityNotFoundException;
import com.telerikacademy.web.peerReview.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.peerReview.models.*;
import com.telerikacademy.web.peerReview.repositories.contracts.WorkItemRepository;
import com.telerikacademy.web.peerReview.services.contracts.FileStore;
import com.telerikacademy.web.peerReview.services.contracts.UserService;
import com.telerikacademy.web.peerReview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.peerReview.services.FileStoreImpl.keyGenerator;
import static java.lang.String.format;

@Service
public class WorkItemServiceImpl implements WorkItemService {

    public static final int STATUS_PENDING_ID = 1;
    public static final int CHANGE_REQUESTED_STATUS_ID = 3;
    public static final int REJECTED_STATUS_ID = 5;
    public static final String UPADTE_ONY_BY_REVIEWER = "You are trying to update workItem that you are not the reviewer of";
    public static final String CANT_SET_PENDING = "You can't set status PENDING!";
    public static final String MUST_ADD_COMMENT_MESSAGE = "If you set status CHANGES REQUESTED or REJECTED you have to add a comment";
    public static final String CANT_DELETE_ITEM_IF_NOT_ACCEPTED = "You can't delete this workItem, because it is not completed!";
    public static final String USER_NOT_CREATOR_MESSAGE = "User %s is not the creator!";
    public static final String ONLY_CREATOR_AND_REVIEWER_MAY_ATTACH = "Only the creator and the reviewer may add attachments to this work item";
    public static final String REVIEWER_NOT_MEMBER_OF_TEAM = "The reviewer %s is not member of the team with id %d";
    public static final String ONLY_CREATOR_AND_REVIEWER_MAY_DOWNLOAD = "Only the creator and the reviewer may download files related to this work item";
    public static final String NO_FILE_TO_DOWNLOAD = "This work item has no files to download";
    public static final String YOU_ARE_NOT_MEMBER = "You are not member of team with name %s";

    private final WorkItemRepository workItemRepository;
    private final FileStore fileStore;
    private final UserService userService;

    @Autowired
    public WorkItemServiceImpl(WorkItemRepository workItemRepository, FileStore fileStore, UserService userService) {
        this.workItemRepository = workItemRepository;
        this.fileStore = fileStore;
        this.userService = userService;
    }

    @Override
    public List<WorkItem> getAll() {
        return workItemRepository.getAll();
    }

    @Override
    public List<WorkItem> listPendingWorkItemsOfTeam(User user, Team team) {
        if (!userService.userIsMemberOfTeam(team, user)) {
            throw new UnauthorizedOperationException(format(YOU_ARE_NOT_MEMBER, team.getName()));
        }
        return workItemRepository.listPendingWorkItemsOfTeam(team.getId());
    }

    @Override
    public List<WorkItem> listUserCreatedWorkItems(int userId) {
        return workItemRepository.listUserCreatedWorkItems(userId);
    }

    @Override
    public List<WorkItem> filter(User user, Optional<String> creatorUsername, Optional<String> reviewerUsername, Optional<Integer> statusId, Optional<Integer> teamId, Optional<String> title, Optional<String> sort) {
        return workItemRepository.filter(creatorUsername, reviewerUsername, statusId, teamId, title, sort);
    }

    @Override
    public byte[] download(User user, WorkItem workItem) {
        if (!(workItem.getReviewer().equals(user) || workItem.getCreator().equals(user))) {
            throw new UnauthorizedOperationException
                    (ONLY_CREATOR_AND_REVIEWER_MAY_DOWNLOAD);
        }
        if (workItem.getFile() == null) {
            throw new EntityNotFoundException(NO_FILE_TO_DOWNLOAD);
        }
        return fileStore.download(workItem.getFile());
    }

    @Override
    public byte[] downloadAttachment(User user, Attachment attachment) {
        if (!(attachment.getWorkItem().getReviewer().equals(user) || attachment.getWorkItem().getCreator().equals(user))) {
            throw new UnauthorizedOperationException
                    (ONLY_CREATOR_AND_REVIEWER_MAY_DOWNLOAD);
        }
        return fileStore.download(attachment.getAttachmentName());
    }

    @Override
    public List<WorkItem> listItemsForReviewByThisUser(int userId) {
        return workItemRepository.listItemsForReviewByThisUser(userId);
    }

    @Override
    public List<WorkItem> listPendingItemsForReviewByThisUser(int userId) {
        return workItemRepository.listPendingItemsForReviewByThisUser(userId);
    }

    @Override
    public WorkItem getById(int id) {
        return workItemRepository.getById(id);
    }

    public int create(WorkItem workItem, Optional<MultipartFile> multipartFile) {
        if (!userService.userIsMemberOfTeam(workItem.getTeam(), workItem.getReviewer())) {
            throw new UnauthorizedOperationException(format(REVIEWER_NOT_MEMBER_OF_TEAM,
                    workItem.getReviewer().getFirstName() + " " + workItem.getReviewer().getLastName(), workItem.getTeam().getId()));
        }
        workItemRepository.create(workItem);
        multipartFile.ifPresent(v -> {
            if (multipartFile.get().isEmpty()) {
                return;
            }
            try {
                String fileKey = keyGenerator(multipartFile.get().getOriginalFilename(), workItem, workItem.getId());
                fileStore.uploadFile(multipartFile.get(), fileKey);
                workItem.setFile(fileKey);
                workItemRepository.update(workItem);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        System.out.print(workItem.getId());
        return workItem.getId();
    }

    @Override
    public void attach(User user, int workItemId, MultipartFile multipartFile) throws IOException {
        WorkItem workItem = workItemRepository.getById(workItemId);
        if (!(workItem.getReviewer().equals(user) || workItem.getCreator().equals(user))) {
            throw new UnauthorizedOperationException
                    (ONLY_CREATOR_AND_REVIEWER_MAY_ATTACH);
        }
        if (multipartFile.isEmpty()) {
            return;
        }
        Attachment attachment = new Attachment();
        attachment.setWorkItem(workItem);
        workItemRepository.createAttachment(attachment);
        String fileKey = keyGenerator(multipartFile.getOriginalFilename(), attachment, attachment.getId());
        fileStore.uploadFile(multipartFile, fileKey);
        attachment.setAttachmentName(fileKey);
        workItemRepository.updateAttachment(attachment);
    }

    @Override
    public void update(User user, WorkItem workItemToUpdate, int statusId, Optional<String> comment) {
        if (!userService.userIsReviewerOfWorkItem(workItemToUpdate, user)) {
            throw new UnauthorizedOperationException
                    (UPADTE_ONY_BY_REVIEWER);
        }
        if (statusId == STATUS_PENDING_ID) {
            throw new ChangesDeniedException(CANT_SET_PENDING);
        }
        if ((statusId == CHANGE_REQUESTED_STATUS_ID || statusId == REJECTED_STATUS_ID) && (comment.isEmpty() || comment.get().equals(""))) {
            throw new ChangesDeniedException
                    (MUST_ADD_COMMENT_MESSAGE);
        }
        workItemToUpdate.setStatus(workItemRepository.getStatusById(statusId));
        if (comment.isPresent() && !comment.get().equals("")) {
            createComment(comment, workItemToUpdate);
        }
        workItemRepository.update(workItemToUpdate);
    }

    void createComment(Optional<String> comment, WorkItem workItemToUpdate) {
        Comment commentToCreate = new Comment();
        commentToCreate.setCommentContent(comment.get());
        commentToCreate.setWorkItem(workItemToUpdate);
        commentToCreate.setTimestamp(LocalDateTime.now());
        workItemRepository.createComment(commentToCreate);
    }

    @Override
    public void delete(User user, int id) {
        WorkItem workItem = getById(id);
        if (!workItem.getStatus().getStatusName().equals("Accepted")) {
            throw new ChangesDeniedException(CANT_DELETE_ITEM_IF_NOT_ACCEPTED);
        }
        if (workItem.getReviewer().equals(user)) {
            throw new UnauthorizedOperationException(format(USER_NOT_CREATOR_MESSAGE,
                    user.getFirstName() + " " + user.getLastName()));
        }
        workItemRepository.delete(id);
    }

    @Override
    public Attachment getAttachmentById(int id) {
        return workItemRepository.getAttachmentById(id);
    }

    @Override
    public List<Attachment> getAllAttachments(int workItemId) {
        return workItemRepository.getAllAttachments(workItemId);
    }

    @Override
    public List<Comment> getAllComments(int workItemId) {
        return workItemRepository.getAllComments(workItemId);
    }

    @Override
    public List<Status> getAllStatuses() {
        return workItemRepository.getAllStatuses();
    }
}
